const Sequelize = require('sequelize');

module.exports = function () {
  const app = this;
  const db = app.get('db');
  const sequelize = new Sequelize(db.url, {
    dialect: db.dialect,
    logging: function (str) {
      console.log(str);
    },
    define: {
      freezeTableName: true
    }
  });

  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args) {
    const result = oldSetup.apply(this, args);
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });
    return result;
  };
};
