//const config = require('../../config');
const kue = require('kue');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const redis = require('redis');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const app = feathers();
app.set('root', path.join(__dirname, '..'));
app.configure(configuration(app.get('root')));
const constants = app.get('constants');
const postgres = require('../postgres');
app.configure(postgres);
const Queue = kue.createQueue();
const modelsLoader = require('../utils/modelsLoader');
const stripeServiceConfiguration = require('../utils/stripeService');
const models = modelsLoader(app);
const sequelize = app.get('sequelizeClient');
app.configure(stripeServiceConfiguration);
const stripeService = app.get('stripeService');
app.setup(app);


function createTransfer(transferAmount, userAccountId, userId, sessionId, tokens) {
  const type = constants.BILLING_TYPES.SESSION_PAYMENT_TO_PROVIDER;
  let transferObj = null;

  return sequelize.transaction((_transaction) => {
    return stripeService.createTransfer(
      transferAmount,
      userAccountId
    ).then(transfer => {
      transferObj = transfer;
      let options = {
        transaction: _transaction
      };
      return models['billings'].createTransfer(transferAmount, userId, type, transfer.id, options);
    }).then((billing) => {
      let options = {
        transaction: _transaction
      };
      return models['billings-sessions'].connectBillingToSession(billing.id, sessionId, options);
    }).then(() => {
      return models['sessions'].closeSession(sessionId);
    }).then(() => {
      _.forEach(tokens, function (token) {
        let options = {
          title: "You were listed money at the rate of " + transferAmount + " CU",
          text: "You were listed money at the rate of " + transferAmount + " CU",
          token: token,
          data: {}
        };

        Queue.create('deliveryNotificationWorker', options).save();
      });

      return Promise.resolve();
    }).catch(err => {
      _transaction.rollback(err);
      if (transferObj !== null) {
        return stripeService.refundCharge(transferAmount, transferObj.id)
          .then(result => {
            //logger.payments('payments', util.format('%s', 'stripe - problem with creating session :', {transferData: transferData, err: err}));
            //return Promise.reject(err);
            return;
          }).catch(err => {
            //logger.payments('payments', util.format('%s', 'stripe - problem with refunding charge :', {transferData: transferData, err: err}));
            //return Promise.reject(err);
            return;
          });
      } else {
        //return Promise.reject(err);
        return;
      }
    });
  });
}

function transferMoney() {
  //todo - check end Session Time
  models['sessions'].getSessionsForPayment()
    .then((sessions) => {
      let paymentSessions = [];
      let currentSession;
      let amount;
      let transfers = [];
      for (const session of sessions) {
        amount = 0;
        currentSession = {
          id: session.id,
          providerId: session.providerId,
          accountId: session.user.stripeAccountId,
          userId: session.user.id,
        };

        for (const billing of session.billings) {
          amount = amount + parseFloat(billing.amount);
        }

        for (const device of session.devices) {
          if (typeof currentSession['tokens'] === 'undefined') {
            currentSession['tokens'] = [];
          }
          currentSession['tokens'].push(device.token);
        }
        currentSession['amount'] = amount;
        paymentSessions.push(currentSession);

        let percent = constants.PROVIDER_PAYMENT_PERCENT;
        let currentAmount = currentSession.amount - ((currentSession.amount / 100) * percent);
        transfers.push(createTransfer(
          currentAmount,
          currentSession.accountId,
          currentSession.userId,
          currentSession.id,
          currentSession.tokens
        ));
      }

      return Promise.all(transfers);
    }).then((results) => {
      process.exit();
    }).catch(err => {
      console.log(err);
      process.exit();
    });
}

transferMoney();
