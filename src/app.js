const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const socketio = require('feathers-socketio');
const swagger = require('feathers-swagger');
const session = require('express-session');
const kue = require('kue');
const redis = require('redis');
const pgNative = require('pg-native');
const RedisNotifier = require('redis-notifier');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');

const authentication = require('./authentication');
const logger = require('./logger.js');
const postgres = require('./postgres');
const stree = require('./utils/s3Service.js');
const deliveryNotificationsService = require('./utils/pushService');
const stripeService = require('./utils/stripeService');
const app = feathers();
const ioHandlers = require('./utils/ioHandlers');

const createModel = require('../src/models/posts.model');

const multer = require('./middleware/multer');

// Load app configuration
app.set('root', path.join(__dirname, '..'));
app.configure(configuration(app.get('root')));

const logsDestination = app.get('logger').logsDestination;

kue.app.listen(app.get('kue').port);

const client = redis.createClient();
app.set('redisClient', client);

// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(session({ secret: 'a4f8071f-c873-4447-8es2' }));
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', feathers.static(app.get('public')));

// Set up Plugins and providers
app.configure(hooks());
app.configure(postgres);
app.configure(stree);
app.configure(deliveryNotificationsService);
app.configure(stripeService);
app.configure(rest());
app.configure(socketio(
  function (io) {
    ioHandlers(io, app);
  }
));

app.use(morgan('dev'));

app.configure(logger(logsDestination));
app.configure(authentication);
app.configure( multer);

app.configure(swagger({
  docsPath: '/docs',
  uiIndex: path.join(app.get('public'), 'swagger/index.html')
}));

app.use('/api/v1/password', feathers.static(app.get('password')));

// Set up our services (see `services/index.js`)

app.configure(services);
// Configure middleware (see `middleware/index.js`) - always has to be last
app.configure(middleware);
app.hooks(appHooks);

module.exports = app;
