const Sequelize = require('sequelize');
const Promise   = require('bluebird');
const fs = require('fs');
const path = require('path');
const bcrypt = require('bcryptjs');
const Chance = require('chance');

const normalizedPath = path.join(__dirname, 'models');
const dbConfig  =  require('../config/migrations');

const { DEFAULT, LONGITUDE, LATITUDE } = require('../config/default.json')['constants'];
const chance = new Chance();

var _dbModels = {};
fs.readdirSync(normalizedPath).forEach(function(file) {
  let key = file.split('.')[0].replace('-','_');
  _dbModels[key] = require('./models/' + file);
});

const genSalt = Promise.promisify(bcrypt.genSalt);
const hash = Promise.promisify(bcrypt.hash);

class testDataGenerator {
  constructor(evn, count, image){
    this.defaultCount = parseInt((count || 10), DEFAULT['COUNT']);
    this.defaultImage = image || DEFAULT['IMAGE'];
    this.evn = evn || 'development';

    const { url, dialect } = dbConfig[this.evn];

    this.sequelizeClient = new Sequelize(url, {
      dialect: dialect,
      logging: false,
      define: {
        freezeTableName: true
      }
    });

    const modelsNames = Object.keys(_dbModels);
    modelsNames.forEach((name)=>  _dbModels[name](this));
  }

  get(key){
    return this[key];
  }

  generateByType(type, argv){
    switch(type){
    case 'users'    : return this.generateUsers(...argv);
    case 'pets'     : return this.generatePetsByUser(...argv);
    case 'sessions' : return this.generateSessionByUser(...argv);
    case 'posts'    : return this.generatePostsByUser(...argv);
    case 'favorites': return this.generateFavoritesByUser(argv[0], argv.slice(1, argv.length));
    case 'all'      : return this.generateAll(...argv);
    default         : process.exit();
    }
  }

  generateAll(count){
    var users = [];
    return this.generateUsers().then((generatedUsers)=>{
      users = generatedUsers;
      return Promise.mapSeries(users, (user)=>{
        return this.generatePostsByUser(user.id, count);
      });
    }).then(()=>{
      return Promise.mapSeries(users, (user)=>{
        return this.generatePetsByUser(user.id, count);
      });
    }).then(()=>{
      var ids = users.map((user)=>{ return user.id; });
      return Promise.mapSeries(users, (user, index)=>{
        let favoriteUsersIds = [].concat(ids).splice(index, 1);
        return this.generateFavoritesByUser(user.id, favoriteUsersIds);
      });
    });
  }

  generateUsers(count){
    const { users } = this.sequelizeClient['models'];
    count = count || this.defaultCount;

    var data = [];

    for(let i = 0; i < count; i++){
      let avalable = {
        from: new Date(),
        to: new Date()
      };
      avalable['from'].setHours(
        chance.integer({
          min: 0,
          max: 12
        })
      );

      avalable['to'].setHours(
        chance.integer({
          min: 12,
          max: 24
        })
      );

      data.push({
        email    : chance.email(),
        adress   : chance.address(),
        longitude: chance.floating({
          min  : LONGITUDE['MIN'],
          max  : LONGITUDE['MAX']
        }),
        latitude: chance.floating({
          min : LATITUDE['MIN'],
          max : LATITUDE['MAX']
        }),
        imageUrl  : this.defaultImage,
        name      : chance.name(),
        isProvider: true,
        isVerified: true,
        countOfReviews: chance.integer()
      });
    }

    return Promise.mapSeries(data,(item) => {
      return genSalt(10).then((salt)=>{
        return hash(item.name, salt);
      }).then(function(password){
        return users.create(Object.assign({
          password: password
        }, item));
      });
    });
  }

  generateFavoritesByUser(userId, favoriteUsersIds){
    return Promise.mapSeries(favoriteUsersIds, (favoriteUserId) =>{
      const { favorites } = this.sequelizeClient['models'];
      return favorites.create({
        userId: userId,
        favoriteUserId: favoriteUserId
      });
    });
  }

  generatePetsByUser(userId, count){
    const { pets } = this.sequelizeClient['models'];
    count = count || this.defaultCount;
    var data = [];
    for(let i = 0; i < count; i++){
      data.push({
        name        : chance.name(),
        breed       : chance.word({ length: count }),
        isVactinated: chance.bool(),
        sex         : chance.bool() ? 'male' : 'female',
        userId      : userId,
        imageUrl    : this.defaultImage
      });
    }

    return Promise.mapSeries(data,(item) => {
      return pets.create(item);
    });
  }

  generateSessionByUser(tax, ownerId, providerId, petsIds){
    const { sessions, pets_sessions } = this.sequelizeClient['models'];

    let now = new Date();
    let startSession = chance.date({ year: now.getFullYear() });
    let endSession = new Date(startSession);
    endSession.setDate(endSession.getDate() + chance.integer({
      min: 1,
      max: DEFAULT['MAX_DAYS_COUNT']
    }));

    let data = {
      tax: chance.floating({
        min  : -tax,
        max  :  tax
      }),
      startPoint: chance.address(),
      longitude: chance.floating({
        min  : LONGITUDE['MIN'],
        max  : LONGITUDE['MAX']
      }),
      latitude: chance.floating({
        min : LATITUDE['MIN'],
        max : LATITUDE['MAX']
      }),
      ownerId      : ownerId,
      providerId   : providerId,
      startDateTime: startSession,
      endDateTime  : endSession
    };

    return sessions.create(data).then((session)=>{
      return Promise.mapSesies(petsIds, (petId)=>{
        let data = {
          petId: petId,
          sessionId: session.id
        };
        return pets_sessions.create(data);
      });
    });
  }

  generatePostsByUser(userId, count){
    const { posts } = this.sequelizeClient['models'];
    count = count || this.defaultCount;
    var data = [];
    for(let i = 0; i < count; i++){
      data.push({
        title    : chance.word({ length: count }),
        text     : chance.word({ length: count * count }),
        userId   : userId,
        imageUrl : this.defaultImage
      });
    }

    return Promise.mapSeries(data,(item) => {
      return posts.create(item);
    });
  }
}

(()=>{
  const { env, argv } = process;
  let generator = new testDataGenerator(env['NODE_ENV'], argv[3]);
  generator.generateByType(argv[2], argv.slice(4, argv.length))
    .then(()=>{
      //console.warn(result);
      process.exit();
    }).catch((error)=>{
      console.warn(error);
      process.exit();
    });
})();

