const authentication = require('feathers-authentication');
const jwt = require('feathers-authentication-jwt');
const local = require('feathers-authentication-local');
const oauth1 = require('feathers-authentication-oauth1');
const oauth2 = require('feathers-authentication-oauth2');
// const Verifier = require('feathers-authentication-oauth2').Verifier;
const { getErrorObject } = require('./errorHandler');
const GoogleTokenStrategy = require('passport-google-token').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const TwitterTokenStrategy = require('passport-twitter-token');
const commonHooks = require('feathers-hooks-common');
const errors = require('feathers-errors');


// class CustomVerifier extends Verifier {
//   // The verify function has the exact same inputs and
//   // return values as a vanilla passport strategy
//   verify(req, accessToken, refreshToken, profile, done) {
//     // do your custom stuff. You can call internal Verifier methods
//     // and reference this.app and this.options. This method must be implemented.
//
//     // the 'user' variable can be any truthy value
//     done(null, {});
//   }
// }



module.exports = function () {
  const app = this;
  const config = app.get('authentication');

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local(config.local));

  app.configure(oauth2(Object.assign({
    name: 'google',
    Strategy: GoogleTokenStrategy,
    // Verifier: CustomVerifier
  }, config.google)));

  app.configure(oauth2(Object.assign({
    name: 'facebook',
    Strategy: FacebookTokenStrategy
  }, config.facebook)));

  app.configure(oauth1(Object.assign({
    name: 'twitter',
    Strategy: TwitterTokenStrategy
  }, config.twitter)));

  function checkExistingUser(hook) {
    const sequelizeClient = hook.app.get('sequelizeClient');
    const userModel = sequelizeClient.models.users;
    if (hook.method == 'create' && hook.path == 'authentication' && hook.data.strategy == 'local') {
      if (!hook.data.email) {
        return Promise.reject(new errors.BadRequest('Error: email is required', {errors: {}}, 400));
      }
      let email = hook.data.email;
      return userModel
        .getUserByEmail(email).then((user) => {
          if (!user) {
            hook.cancelled = true;
            hook.result = {accessToken: null, isNew: true};
          }
          return hook;
        });
    }
    return hook;
  }

  function checkExistingUserOauth(hook) {
    const sequelizeClient = hook.app.get('sequelizeClient');
    const userModel = sequelizeClient.models.users;
    if (hook.method == 'create' && hook.path == 'authentication' && hook.data.strategy !== 'local') {
      if(!hook.params.user.lastActivity){
        hook.result.isNew =true;
        userModel.update({
          lastActivity: new Date()
        }, {
          where: {
            id: hook.params.user.id
          }
        }).then(() => {
          hook.result.isNew = true;
          return hook;
        });
      }


    }
    return hook;
  }




  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service('authentication').hooks({
    before: {
      create: [
        checkExistingUser,
        commonHooks.iffElse(hook => hook.cancelled, [], authentication.hooks.authenticate(config.strategies))
      ],
      remove: [
        authentication.hooks.authenticate('jwt')
      ]
    },
    after: {
      create: [
        checkExistingUserOauth
      ]
    },
    error: {
      all: [
        function (hook) {
          hook.error = getErrorObject(hook.error);
        }
      ],
      find: [],
      get: [],
      create: [],
      update: [],
      patch: [],
      remove: []
    }
  });
};
