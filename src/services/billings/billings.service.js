const path = require('path');
const hooks = require('./billings.hooks');
const filters = require('./billings.filters');
const createBillingsModel = require('../../models/billings.model');

class Billing {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.billingsModel = createBillingsModel(app);
  }

  find(params) {
    const {SEARCH} = this.constants;
    console.log(this.constants);
    const {query} = params;
    const {limit, page, offset} = query;

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    return this.billingsModel
      .getBillings(_limit, _offset)
      .then(billings => {
        if (billings == null) {
          return [];
        }
        return billings;
      });
  }
}

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');
  const routePrefix = app.get('prefix');

  const routes = {
    basic: path.join(routePrefix, 'billings'),
  };

  app.use(path.join('/', routes['basic']), new Billing());
  const service = app.service(routes['basic']);
  service.hooks(hooks);


  if (service.filter) {
    service.filter(filters);
  }
};
