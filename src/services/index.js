const users = require('./users/users.service.js');

const admins = require('./admins/admins.service.js');

const billingTypes = require('./billing-types/billing-types.service.js');

const billings = require('./billings/billings.service.js');

const devices = require('./devices/devices.service.js');

const favorites = require('./favorites/favorites.service.js');

const pets = require('./pets/pets.service.js');

const postComments = require('./post-comments/post-comments.service.js');

const postLikes = require('./post-likes/post-likes.service.js');

const posts = require('./posts/posts.service.js');

//const sessionActivities = require('./session-activities/session-activities.service.js');

const sessions = require('./sessions/sessions.service.js');

const settings = require('./settings/settings.service.js');

const postAbuses = require('./post-abuses/post-abuses.service.js');

const postCommentAbuses = require('./post-comment-abuses/post-comment-abuses.service.js');

const sessionMessages = require('./session-messages/session-messages.service.js');

const tempSessions = require('./temp-sessions/temp-sessions.service.js');

const deliveryNotifications = require('./delivery-notifications/delivery-notifications.service.js');


const petsTempSessions = require('./pets-temp-sessions/pets-temp-sessions.service.js');


const billingsSessions = require('./billings-sessions/billings-sessions.service.js');


const mailer = require('./mailer/mailer.service.js');


const password = require('./password/password.service.js');


module.exports = function () {
  const app = this;
  app.configure(devices);
  app.configure(pets);
  app.configure(users);
  app.configure(settings);
  app.configure(favorites);
  app.configure(billings);
  app.configure(sessions);
  app.configure(admins);
  app.configure(billingTypes);
  app.configure(postCommentAbuses);
  app.configure(postComments);
  app.configure(postLikes);
  app.configure(postAbuses);
  app.configure(posts);
  //app.configure(sessionActivities);
  app.configure(sessionMessages);
  app.configure(tempSessions);
  app.configure(deliveryNotifications);
  app.configure(petsTempSessions);
  app.configure(billingsSessions);
  app.configure(mailer);
  app.configure(password);
};
