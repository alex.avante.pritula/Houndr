const { authenticate } = require('feathers-authentication').hooks;
const errors = require('feathers-errors');
const { getErrorObject } = require('../../errorHandler');
const validators = require('../../utils/validators');
const formatters = require('../../utils/formatters');

function validate(hook) {
  let validateErrors = '';
  if (hook.method === 'create' && hook.path === 'api/v1/posts/comments') {
    validateErrors = validators.postComment.create(hook.data);
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }

  return hook;
}

function formateResponse(hook) {
  /*if (hook.method === 'get' && hook.path === 'api/v1/posts/comments') {
    hook.result = formatters.postComment.getPostComment(hook.result);
  }*/

  /*if (hook.method === 'find' && hook.path === 'api/v1/posts/comments') {
    hook.result = formatters.postComment.getPostComments(hook.result);
  }*/

  return hook;
}

module.exports = {
  before: {
    all: [ authenticate('jwt'), validate ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [formateResponse],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
