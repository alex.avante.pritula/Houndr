const createModel = require('../../models/post-comments.model');
const createUserModel = require('../../models/users.model');

const hooks = require('./post-comments.hooks');
const filters = require('./post-comments.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const UserModel = createUserModel(app);
  const {SEARCH} = app.get('constants');
  const sequelize = app.get('sequelizeClient');
  const s3Service = app.get('s3Service');

  class PostComments {
    setup(app) {
      this.app = app;
      this.sequelize = app.get('sequelizeClient');
    }

    create(data, params) {
      let jwtData = params.payload;
      let response;
      return this.sequelize.transaction(t => {
        return Model.create({
          postId: data.post_id,
          userId: jwtData.userId,
          text: data.text,
          img_url:data.img_url
        }, {transaction: t})
          .then((result) => {
            response = result;
            return this.sequelize.models.posts.update({
              new_comments: true
            }, {
              where: {
                id: data.post_id
              },
              transaction: t
            });
          })
          .then(() => {
            return response;
          });
      });
    }

    get(id) {
      return Model.findOne({
        where: {id: id},
        include: [{
          model: UserModel,
          'as': 'user',
          attributes: [
            'id',
            'image_url',
            ['name', 'username']
          ]
        }],
        attributes: [
          'id',
          'text',
          'created_at',
          'updated_at'
        ]
      }).then(comment => {
        if (comment == null) {
          return {}
        }
        return comment;
      });
    }

    find(params) {
      const {query} = params;
      const {limit, page, offset} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.findAll({
        limit: _limit,
        offset: _offset,
        include: [{
          model: UserModel,
          'as': 'user',
          attributes: [
            'id',
            'image_url',
            ['name', 'username']
          ]
        }],
        attributes: [
          'id',
          'text',
          'created_at',
          'updated_at'
        ]
      }).then(comments => {
        if (comments == null) {
          return [];
        }
        return comments;
      });
    }
  }

  app.use('/api/v1/posts/comments', new PostComments());
  const service = app.service('api/v1/posts/comments');

  app.use('/api/v1/posts/comments/image', {
    create(data, params) {
      let {files} = params
      return s3Service.uploadImages('comments', files)
        .then(result => {
          return {url: result[0].url};
        });
    }
  });

  //get comments by postId

  app.use('/api/v1/posts/:post_id/comments', {

    find(params) {

      const {query} = params;
      const {limit, page, offset} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      let response;
      return sequelize.transaction((t) => {
        return Model.findAll({
          where: {
            post_id: params.post_id
          },
          limit: _limit,
          offset: _offset,
          include: [{
            model: UserModel,
            'as': 'user',
            attributes: [
              'id',
              'image_url',
              'name',
              'last_name'
            ]
          }],
          raw: true,
          transaction: t
        })
          .then(results => {
            if (results == null) {
              response = [];
            }
            response = results;
            return sequelize.models.posts.update(
              {new_comments: false},
              {
                where: {
                  id: params.post_id
                }
              },
              {transaction: t}
            );
          })
          .then(() => {
            return response;
          });
      });

    }
  });


  const commentsPostService = app.service('api/v1/posts/:post_id/comments');
  const commentsImageService = app.service('/api/v1/posts/comments/image');

  service.hooks(hooks);
  commentsPostService.hooks(hooks);
  commentsImageService.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
