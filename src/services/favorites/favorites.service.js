const createModel = require('../../models/favorites.model');
const createUserModel = require('../../models/users.model');
const hooks = require('./favorites.hooks');
const filters = require('./favorites.filters');
const path = require('path');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const UserModel = createUserModel(app);
  const { SEARCH } = app.get('constants');
  const route = path.join(app.get('prefix'), 'favorites');
  const sequelizeClient = app.get('sequelizeClient');

  class Favorites {

    setup(app){
      this.app = app;
    }

    find(params){
      const { userId  } = params.payload;

      const { limit, offset, page } = params.query;
      const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
      const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10)   || _page * _limit;

      let options = {
        limit:  _limit,
        offset: _offset,
        where: { userId: userId },
        include : [{
          model: UserModel,
          as: 'favoriteUser',
          attributes: [
            'id',
            'image_url',
            'name', 
            'latitude',
            'longitude',
            'count_reviews',
            [
              sequelizeClient.literal(`( 
                  SELECT AVG("session_details"."rating") FROM "session_details" 
                  WHERE "session_details"."user_id" = "favoriteUser".id AND "session_details".account_type = 'provider'
              )`), 'rating'
            ]
          ]
        }]
      };

      return Model.findAll(options);
    }

    create(data, params){
      const { userId  } = params.payload;
      const { favoriteUserId } = data;

      let options = {
        userId: userId,
        favoriteUserId: favoriteUserId
      };

      if(userId === favoriteUserId) {
          return Promise.reject(new errors.BadRequest('Invalid data', { errors: options }, 400));
      }

      return UserModel.find({ where: {id: favoriteUserId }})
      .then((favoriteUser)=>{
        if(!favoriteUser){
          return Promise.reject(new errors.BadRequest('Invalid favorite user Id', { errors: { favoriteUserId: favoriteUserId } }, 400));
        }
        return Model.find({ where: options });
      }).then((existFavorite)=>{
          if(existFavorite){
             return Promise.reject(new errors.BadRequest('Favorite user already binded', { errors: options }, 400));
          }
          return Model.create(options);
      }).then(()=>{
          return {};
      });
    }

    remove(id){
      return Model.findById(id)
        .then((user)=>{
          if(!user){
            return Promise.reject(new errors.BadRequest('Invalid favorite user Id', { errors: { id: id } }, 400));
          }
          return user.destroy();
        });
    }

  }

  app.use(path.join('/',route), new Favorites());
  const service = app.service(route);

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
