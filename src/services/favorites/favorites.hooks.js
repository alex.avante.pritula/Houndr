const { authenticate } = require('feathers-authentication').hooks;
const { getErrorObject } = require('../../errorHandler');
const formatters = require('../../utils/formatters');
const path = require('path');

function formateResponse(hook) {
  const route = path.join(hook.app.get('prefix'), 'favorites');
  if (hook.method === 'find' && hook.path === route) {
    hook.result = formatters.user.getFavoriteUsers(hook.result);
  }
  return hook;
}


module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [ formateResponse ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
