const createService = require('feathers-sequelize');
const createModel = require('../../models/admins.model');
const hooks = require('./admins.hooks');
const filters = require('./admins.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'admins',
    Model,
    paginate
  };

  app.use('/admins', createService(options));
  const service = app.service('admins');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
