const createModel = require('../../models/post-comment-abuses.model');
const hooks = require('./post-comment-abuses.hooks');
const filters = require('./post-comment-abuses.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const { SEARCH } = app.get('constants');

  class PostCommentAbuses {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { query } = params;
      const { limit, page, offset } = query;

      const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
      const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10)   || _page * _limit;

      return Model.findAll({
        limit: _limit,
        offset: _offset,
        raw: true
      }).then(postAbuses => {
        if (postAbuses == null) {
          return [];
        }
        return postAbuses;
      });
    }

    create(data, params) {
      let jwtData = params.payload;
      return Model.create({
        text: data.text,
        postCommentId: data.post_comment_id,
        userId: jwtData.userId,
        raw: true
      });
    }
  }

  app.use('/api/v1/posts/comments/abuses', new PostCommentAbuses());
  const service = app.service('api/v1/posts/comments/abuses');

  app.use('/api/v1/posts/comments/:post_comment_id/abuses', {
    find(params) {
      return Model.findAll({
        where: {post_comment_id: params.post_comment_id}
      }).then(abuses => {
        if (abuses == null) {
          return {};
        }
        return abuses;
      });
    }
  });

  const abusesCertainPostCommentService = app.service('api/v1/posts/comments/:post_comment_id/abuses');

  service.hooks(hooks);
  abusesCertainPostCommentService.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
