// Initializes the `session-messages` service on path `/session-messages`
const createService = require('feathers-sequelize');
const path = require('path');
const createModel = require('../../models/session-messages.model');
const hooks = require('./session-messages.hooks');
const filters = require('./session-messages.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const routePrefix = app.get("prefix");
  const routes = {
    posts: path.join(routePrefix,'posts'),
  };

  const paginate = app.get('paginate');
  const { SEARCH } = app.get("constants");

  const options = {
    name: 'session_messages',
    Model,
    paginate
  };

  class SessionMessages {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { payload, query } = params;
      const { limit, page, offset } = query;

      const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
      const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10)   || _page * _limit;

      return Model.getMessages(_limit, _offset, payload.userId);
    }
  }

  // Initialize our service with any options it requires
  app.use('/session-messages', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('session-messages');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
