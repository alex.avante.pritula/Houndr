// Initializes the `password` service on path `/password`

const hooks = require('./password.hooks');
const filters = require('./password.filters');
const errors = require('feathers-errors');
const error = require('feathers-errors/handler');
const path = require('path');
const feathers = require('feathers');


module.exports = function () {
  const app = this;
  app.use(error({}));


  app.get('/api/v1/password/:token', function (req, res, next) {
    const sequelize = app.get('sequelizeClient');
    let token = req.params.token;
    return sequelize.models.users.findByToken(token)
      .then(user => {
        if (user === null) {
          return res.redirect('/404');
        }
        return res.sendFile(path.join(__dirname, '../../../public/password', 'index.html'));
      });
  });

//$2a$10$tmLIVMwWgrZLYkRioiehWu.cEnvAt.Go5byVUtxVsPmz9Zj.NFPGe
  app.use('/api/v1/password/', {
    update(id, data, params) {
      const sequelize = app.get('sequelizeClient');
      return sequelize.models.users.findByToken(data.token)
        .then(user => {
          if (user === null) {
            return Promise.reject(new errors.NotAuthenticated());
          }
          return user.update({
            password: data.password,
            restore_token: null
          });
        })
        .then(() => {
          return {message: 'ok'};
        });
    }
  });


  const service = app.service('/api/v1/password/');


  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
