// Initializes the `delivery-notifications` service on path `/delivery-notifications`
const createService = require('feathers-sequelize');
const createModel = require('../../models/delivery-notifications.model');
const hooks = require('./delivery-notifications.hooks');
const filters = require('./delivery-notifications.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'delivery-notifications',
    Model,
    paginate
  };

  class DeliveryNotifications {
    setup(app) {
      this.app = app;
    }
  }


  // Initialize our service with any options it requires
  app.use('/delivery-notifications', new DeliveryNotifications());

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('delivery-notifications');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
