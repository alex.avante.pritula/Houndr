const createService = require('feathers-sequelize');
const createModel = require('../../models/session-activities.model');
const hooks = require('./session-activities.hooks');
const filters = require('./session-activities.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'session-activities',
    Model,
    paginate
  };

  app.use('/session-activities', createService(options));
  const service = app.service('session-activities');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
