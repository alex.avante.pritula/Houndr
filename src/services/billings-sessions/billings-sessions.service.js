// Initializes the `billings-sessions` service on path `/billings-sessions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/billings-sessions.model');
const hooks = require('./billings-sessions.hooks');
const filters = require('./billings-sessions.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'billings-sessions',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/billings-sessions', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('billings-sessions');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
