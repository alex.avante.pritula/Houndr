const createService = require('feathers-sequelize');
const createModel = require('../../models/billing-types.model');
const hooks = require('./billing-types.hooks');
const filters = require('./billing-types.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'billing-types',
    Model,
    paginate
  };

  app.use('/billing-types', createService(options));

  const service = app.service('billing-types');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
