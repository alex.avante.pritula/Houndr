const path = require('path');
const createService = require('feathers-sequelize');

const hooks = require('./pets.hooks');
const createModel = require('../../models/pets.model');

class PetImageService {
  setup(app){
    this.app = app;
    this.PetModel = createModel(app);
  }

  create(data, params){
    const s3Service = this.app.get('s3Service');
    const { files, id } = params;
    var currentPet = null;
    return this.PetModel.findById(id).then((pet)=>{
      currentPet = pet;
      return s3Service.uploadImage('pets', files[0]);
    }).then((s3Image)=>{
      currentPet.imageUrl = s3Image.url;
      currentPet.save();
    });
  }

  patch(_id, _data, params){
    const s3Service = this.app.get('s3Service');
    const { files, id } = params;
    var data = {
      pet    : null,
      s3Image: null
    };

    return this.PetModel.findById(id).then((pet)=>{
      data.pet = pet;
      return s3Service.uploadImage('pets', files[0]);
    }).then((s3Image)=>{
      data.s3Image = s3Image;
      const { imageUrl } = data.pet;
      let urlParams =  imageUrl.split('/');
      return s3Service.deleteImage('pets',  urlParams[urlParams.length - 1]  );
    }).then(()=>{
      const { pet, s3Image } = data;
      pet.imageUrl = s3Image.url;
      return pet.save();
    });
  }
}

class UserPets {
  setup(app) {
    this.app = app;
    this.petsModel = createModel(app);
  }
  create(data){
    return this.petsModel.create(data);
  }
  find(params) {
    const { SEARCH } = this.app.get('constants');
    const { payload, query } = params;
    const { userId } = payload;
    const { limit, page, offset } = query;

    const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
    const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10)   || _page * _limit;

    return this.petsModel.getUserPets(userId, _limit, _offset);
  }
}

module.exports = function () {
  const app = this;
  const Model = createModel(app);

  const paginate = app.get('paginate');
  const routePrefix = app.get('prefix');

  const routes = {
    basic: path.join(routePrefix, 'pets'),
    image: path.join(routePrefix, 'pets/:id/image'),
    user_pets: path.join(routePrefix, 'users/pets'),
  };

  const options = {
    name: 'pets',
    Model,
    paginate
  };

  app.use(path.join('/',routes['basic']), createService(options));
  const service = app.service(routes['basic']);
  service.hooks(hooks);

  app.use(path.join('/',routes['user_pets']), new UserPets());
  const userPetsService = app.service(routes['user_pets']);
  userPetsService.hooks(hooks);

  app.use(path.join('/',routes['image']), new PetImageService());
  const petImageService = app.service(routes['image']);
  petImageService.hooks(hooks);
};
