const { authenticate } = require('feathers-authentication').hooks;
const { getErrorObject } = require('../../errorHandler');
const validators = require('../../utils/validators');
const errors = require('feathers-errors');

function validate(hook) {
  let validateErrors = ''
  ;
  if (hook.method === 'create' && hook.path === 'api/v1/users/pets') {
    validateErrors = validators.pet.create(hook.data);
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }
  return hook;
}

module.exports = {
  before: {
    all: [ authenticate('jwt') ,validate],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
