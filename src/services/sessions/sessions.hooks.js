const { authenticate } = require('feathers-authentication').hooks;
const path = require('path');
const validators = require('../../utils/validators');
//const formatters = require('../../utils/formatters');
const { getErrorObject } = require('../../errorHandler');
const errors = require('feathers-errors');

function validate(hook) {
  let validateErrors = '';
  if (hook.method === 'create' && hook.path === 'api/v1/sessions') {
    validateErrors = validators.session.create(hook.data);
  }

  if (hook.method === 'patch' && hook.path === 'api/v1/sessions/prolong') {
    validateErrors = validators.session.payProlong(hook.data);
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }

  return hook;
}

module.exports = {
  before: {
    all: [ authenticate('jwt'), validate ],
    find: [
      function validate(hook) {
        /* let validateErrors = [];
        const routePrefix = hook.app.get("prefix");
        if (hook.method === 'find' && hook.path === path.join(routePrefix, 'sessions/type/:type')) {
            validators.session.getSessionByTypes(hook.params);
        }

        if (validateErrors.length > 0) {
          //return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors.toString(), {errors: validateErrors}, 400));
        } */
        return hook;
      }
    ],
    get: [],
    create: [
      function validate(hook) {
        let validateErrors = [];
        const routePrefix = hook.app.get('prefix');
        const createReview = validators.sessionDetails.create;
        if (hook.path === path.join(routePrefix,'sessions/:session_id/review')) {
          validateErrors = createReview(hook.data);
        }

        if (validateErrors.length) {
          return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors.toString(), { errors: validateErrors }, 400));
        }

        return hook;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
