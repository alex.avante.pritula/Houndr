const path = require('path');
const Promise = require('bluebird');
const moment = require('moment');
const momentTimezone = require('moment-timezone');
const _ = require('lodash');
const errors = require('feathers-errors');
const createService = require('feathers-sequelize');

const hooks = require('./sessions.hooks');
const createPetsSessionsModel = require('../../models/pets-sessions.model');
const createSessionModel = require('../../models/sessions.model');
const createSessionDetailsModel = require('../../models/session-details.model');
const createSessionMessagesModel = require('../../models/session-messages.model');
const createTempSessionsModel = require('../../models/temp-sessions.model');
const createDevicesModel = require('../../models/devices.model');
const createTempSessionProvidersModel = require('../../models/temp-session-providers.model');
const createBillingsModel = require('../../models/billings.model');
const createUsersModel = require('../../models/users.model');
const createBillingsSessionsModel = require('../../models/billings-sessions.model');

const kue = require('kue');
const Queue = kue.createQueue();

class Session {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.sessionModel = createSessionModel(app);
    this.petsSessionsModel = createPetsSessionsModel(app);
    this.tempSessionModel = createTempSessionsModel(app);
    this.devicesModel = createDevicesModel(app);
    this.billingsModel = createBillingsModel(app);
    this.usersModel = createUsersModel(app);
    this.billingsSessionsModel = createBillingsSessionsModel(app);
  }

  create(data, params) {
    const {payload} = params;
    const {userId} = payload;
    const sequelize = this.app.get('sequelizeClient');
    const redisClient = this.app.get('redisClient');
    const stripeService = this.app.get('stripeService');

    //validate data - temp_session_id, provider_id, amount - required

    let tempSessionObj = null;
    let sessionObj = null;
    let userObj = null;
    let chargeObj = null;
    let amount = data.amount;//todo - when we will pay we will count amount
    let billingType = this.constants.BILLING_TYPES.SESSION_OWNER_PAYMENT;

    return this.usersModel
      .getUserById(userId)
      .then((user) => {
        userObj = user;
        if (!userObj.stripeCustomerId.length) {
          return Promise.reject(new errors.BadRequest('User does not have stripe customer', {errors: {}}, 400));
        }

        return this.tempSessionModel.getTempSessionById(data.temp_session_id);
      }).then(tempSession => {
        if (!tempSession) {
          return Promise.reject(new errors.BadRequest('Invalid temp session Id', {errors: {id: data.temp_session_id}}, 400));
        }

        tempSessionObj = tempSession;

        return sequelize.transaction((_transaction) => {
          return this.sessionModel.create({
            providerId: data.provider_id,
            ownerId: tempSessionObj.ownerId,
            tax: amount,
            startDateTime: tempSessionObj.startDateTime,
            endDateTime: tempSessionObj.endDateTime,
            longitude: tempSessionObj.longitude,
            latitude: tempSessionObj.latitude
          }, {
            transaction: _transaction
          }).then((session) => {
            sessionObj = session;
            let pets = tempSessionObj.pets_temp_sessions.map((pet) => {
              return {
                sessionId: session.id,
                petId: pet.pet_id
              };
            });

            //connect pets to session
            return this.petsSessionsModel.bulkCreate(pets, {
              transaction: _transaction
            });
          }).then((result) => {
            //remove temp session
            console.log('remove temp session');
            return this.tempSessionModel.destroy({
              where: {
                id: tempSessionObj.id
              }
            }, {
              transaction: _transaction
            });
          }).then(result => {
            console.log('create charge');
            return stripeService.createCharge(amount, userObj.stripeCustomerId);
          }).then(charge => {
            chargeObj = charge;

            let options = {
              transaction: _transaction
            };

            return this.billingsModel.createCharge(userId, amount, billingType, charge.id, options);
          }).catch(err => {
            _transaction.rollback(err);
            if (chargeObj !== null) {
              return stripeService.refundCharge(amount, chargeObj.id)
                .then(result => {
                  //logger.payments('payments', util.format('%s', 'stripe - problem with creating session :', {transferData: transferData, err: err}));
                  return Promise.reject(err);
                }).catch(err => {
                  //logger.payments('payments', util.format('%s', 'stripe - problem with refunding charge :', {transferData: transferData, err: err}));
                  return Promise.reject(err);
                });
            } else {
              return Promise.reject(err);
            }
          });
        });
      }).then(result => {
        return this.billingsSessionsModel.connectBillingToSession(result.id, sessionObj.id);
      }).then(result => {
        return this.devicesModel.getUserDevicesById(sessionObj.ownerId);
      }).then((devices) => {
        if (devices === null) {
          devices = [];
        }

        /*notify owner prolong session*/
        let countMinutesBeforeNotifyOwner = 30;
        let countSecondsNotificationStartSession = ((moment().diff(sessionObj.endDateTime, 'seconds')) * (-1)) - (countMinutesBeforeNotifyOwner * 60);

        /*notify provider for a walk dog*/
        let countMinutesBeforeNotifyProvider = 30;
        let countSecondsNotificationEndSession = ((moment().diff(sessionObj.startDateTime, 'seconds')) * (-1)) - (countMinutesBeforeNotifyProvider * 60);

        //set redis notification
        let jsonObj = {
          type: "NotifyBeforeWalk",
          providerId: sessionObj.providerId,
          sessionId: sessionObj.id
        };

        let redisKey = "custom_" + JSON.stringify(jsonObj);
        redisClient.set(redisKey, jsonObj, function (err, reply) {
        });
        redisClient.expire(redisKey, countSecondsNotificationStartSession);

        jsonObj = {
          type: "NotifyProlongSession",
          ownerId: sessionObj.ownerId,
          sessionId: sessionObj.id
        };

        redisKey = "custom_" + JSON.stringify(jsonObj);
        redisClient.set(redisKey, jsonObj, function (err, reply) {
        });
        redisClient.expire(redisKey, countSecondsNotificationEndSession);

        //Send PN to Provider that was created session;
        _.forEach(devices, function (device) {
          let options = {
            title: "Session was created",
            text: "Session was created",
            token: device.token,
            data: {
              sessionId: sessionObj.id
            }
          };

          Queue.create('deliveryNotificationWorker', options).save();
        });

        return [];
      }).catch(err => {
        return Promise.reject(new errors.BadRequest('problem with creating session: ', {errors: err}, 400));
      });
  }

  get(id) {
    return this.sessionModel.getSessionById(id).then((session)=>{
     if (session === null) {
     return [];
     }
     return session.toJSON();
     });
  }

  /*return sessions for owner*/
  find(params) {
    const {payload, query} = params;
    const {userId} = payload;
    const {limit, page, offset} = query;
    const {SEARCH} = this.app.get('constants');

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    let options = {
      limit: _limit,
      offset: _offset,
      where: {owner_id: userId},
    };

    return this.sessionModel.findAll(options);
  }
}

class SessionProviders {
  setup(app) {
    this.app = app;
    this.sessionModel = createSessionModel(app);
  }

  /*return sessions for provider*/
  find(params) {
    const {payload, query} = params;
    const {userId} = payload;
    const {limit, page, offset} = query;
    const {SEARCH} = this.app.get('constants');

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    let options = {
      limit: _limit,
      offset: _offset,
      where: {provider_id: userId},
    };

    return this.sessionModel.findAll(options);
  }
}

class SessionProlong {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.sessionModel = createSessionModel(app);
    this.sessionDetailsModel = createSessionDetailsModel(app);
    this.providerModel = createTempSessionProvidersModel(app);
    this.devicesModel = createDevicesModel(app);
    this.usersModel = createUsersModel(app);
    this.billingsModel = createBillingsModel(app);
    this.billingsSessionsModel = createBillingsSessionsModel(app);
  }

  /*owner add invitation to prolong session to provider*/
  create(data, params) {
    const {payload} = params;
    const {userId} = payload;
    let sessionObj = null;

    //validate id of session, prolong_time

    return this.sessionModel.findById(data.id).then((session) => {
      if (session === null) {
        return Promise.reject(new errors.BadRequest('Invalid session Id', {errors: {id: id}}, 400));
      }

      sessionObj = session;

      const {ownerId, providerId} = session;

      let accountType = null;
      if (userId === ownerId) {
        accountType = 'owner';
      }

      if (accountType !== 'owner') {
        return Promise.reject(new errors.BadRequest('Invalid current user', {errors: {userId: userId}}, 400));
      }

      //check end session
      if (moment().diff(session.endDateTime, 'seconds') > 0) {
        return Promise.reject(new errors.BadRequest('Session time is over', {errors: {}}, 400));
      }

      //check provider has exist sessions or temp sessions in this time
      return this.providerModel.getCountSessionsByRangeDate(
        sessionObj.startDateTime,
        sessionObj.endDateTime,
        sessionObj.providerId,
        null,
        sessionObj.id
      ).then(sessions => {
        if (sessions[0].count > 0) {
          return Promise.reject(new errors.BadRequest('Provider has sessions in same time', {errors: {}}, 400));
        }

        return this.devicesModel
          .getUserDevicesById(sessionObj.providerId)
          .then((devices) => {
            //Send PN to provider invitation to prolong session
            _.forEach(devices, function (device) {
              let options = {
                title: "You were invited to prolong session",
                text: "You were invited to prolong session",
                token: device.token,
                data: {
                  providerId: sessionObj.providerId,
                  ownerId: sessionObj.ownerId,
                  sessionId: sessionObj.id,
                  prolongTime: data.prolong_time
                }
              };

              Queue.create('deliveryNotificationWorker', options).save();
            });
            return [];
          });
      });
    });
  }

  /*pay and prolong*/
  patch(id, data, params) {
    const {payload} = params;
    const {userId} = payload;
    const stripeService = this.app.get('stripeService');
    const sequelize = this.app.get('sequelizeClient');

    let sessionObj = null;
    let chargeObj = null;
    let userObj = null;
    let billingObj = null;
    let amount = data.amount;
    let billingType = this.constants.BILLING_TYPES.PROLONG_SESSION_OWNER_PAYMENT;

    //todo pay
    //prolong_time
    return this.sessionModel.findById(id).then((session) => {
      if (session === null) {
        return Promise.reject(new errors.BadRequest('Invalid session Id', {errors: {id: id}}, 400));
      }

      sessionObj = session;

      const {ownerId, providerId} = session;
      let accountType = null;
      if (userId === ownerId) {
        accountType = 'owner';
      }

      if (accountType !== 'owner') {
        return Promise.reject(new errors.BadRequest('Invalid current user', {errors: {userId: userId}}, 400));
      }

      //check provider has exist sessions or temp sessions in this time
      return this.usersModel
        .getUserById(userId)
        .then((user) => {
          userObj = user;
          if (!userObj.stripeCustomerId.length) {
            return Promise.reject(new errors.BadRequest('User does not have stripe customer', {errors: {}}, 400));
          }

          return this.providerModel.getCountSessionsByRangeDate(
            sessionObj.startDateTime,
            sessionObj.endDateTime,
            sessionObj.providerId,
            null,
            sessionObj.id
          );
        }).then(sessions => {
            if (sessions[0].count > 0) {
              return Promise.reject(new errors.BadRequest('Provider has sessions in same time', {errors: {}}, 400));
            }

            //check end session
            if (moment().diff(session.endDateTime, 'seconds') > 0) {
              return Promise.reject(new errors.BadRequest('Session time is over', {errors: {}}, 400));
            }

            //update session
            session.endDateTime = moment(session.endDateTime).add(data.prolong_time, 'm');
            return sequelize.transaction((_transaction) => {
              return session.save({transaction: _transaction})
                .then(result => {
                  return stripeService.createCharge(amount, userObj.stripeCustomerId);
                }).then(charge => {
                  chargeObj = charge;

                  let options = {
                    transaction: _transaction
                  };

                  return this.billingsModel.createCharge(userId, amount, billingType, charge.id, options);
                }).then(billing => {
                  billingObj = billing;
                  //send push notification to provider that session was prolonged
                  return this.devicesModel
                    .getUserDevicesById(sessionObj.ownerId)
                    .then((devices) => {
                      //Send PN to Provider that session was prolonged//silent
                      _.forEach(devices, function (device) {
                        let options = {
                          text: "Provider was agreed on prolong session",
                          token: device.token,
                          data: {
                            providerId: sessionObj.providerId,
                            ownerId: sessionObj.ownerId,
                            sessionId: sessionObj.id
                          }
                        };

                        Queue.create('deliveryNotificationWorker', options).save();
                      });
                      return [];
                    });
                }).catch(err => {
                  _transaction.rollback(err);
                  if (chargeObj !== null) {
                    return stripeService.refundCharge(amount, chargeObj.id)
                      .then(result => {

                        //logger.payments('payments', util.format('%s', 'stripe - problem with creating session :', {transferData: transferData, err: err}));
                        return Promise.reject(new errors.BadRequest('problem with prolonging session: ', {errors: err}, 400));
                      }).catch(err => {
                        //logger.payments('payments', util.format('%s', 'stripe - problem with refunding charge :', {transferData: transferData, err: err}));
                        return Promise.reject(new errors.BadRequest('problem with prolonging session: ', {errors: err}, 400));
                      });
                  } else {
                    return Promise.reject(new errors.BadRequest('problem with prolonging session: ', {errors: err}, 400));
                  }
                });
            }).then(() => {
              return this.billingsSessionsModel.connectBillingToSession(billingObj.id, sessionObj.id);
            })
          });
    });
  }
}

class SessionProlongStatus {
  setup(app) {
    this.app = app;
    this.sessionModel = createSessionModel(app);
    this.sessionDetailsModel = createSessionDetailsModel(app);
    this.providerModel = createTempSessionProvidersModel(app);
    this.devicesModel = createDevicesModel(app);
  }

  /*
   * provider confirm or reject prolong session
   * */
  patch(id, data, params) {
    const {payload, session_id} = params;
    const {userId} = payload;

    let sessionObj = null;
    return this.sessionModel.findById(session_id).then((session) => {
      if (session === null) {
        return Promise.reject(new errors.BadRequest('Invalid session Id', {errors: {id: id}}, 400));
      }

      sessionObj = session;
      const {ownerId, providerId} = session;
      let accountType = null;
      if (userId === providerId) {
        accountType = 'provider';
      }

      if (accountType !== 'provider') {
        return Promise.reject(new errors.BadRequest('Invalid current user', {errors: {userId: userId}}, 400));
      }

      //check end session
      if (moment().diff(session.endDateTime, 'seconds') > 0) {
        return Promise.reject(new errors.BadRequest('Session time is over', {errors: {}}, 400));
      }

      //check provider has exist sessions or temp sessions in this time
      return this.providerModel.getCountSessionsByRangeDate(
        sessionObj.startDateTime,
        sessionObj.endDateTime,
        sessionObj.providerId,
        null,
        sessionObj.id
      ).then(sessions => {
        if (sessions[0].count > 0) {
          return Promise.reject(new errors.BadRequest('You have sessions in same time', {errors: {}}, 400));
        }

        if (data.status && data.status === 'agree') {
          //send push notification to owner that provider agree //silent
          return this.devicesModel
            .getUserDevicesById(sessionObj.ownerId)
            .then((devices) => {
              // TODO => check silent push
              //Send PN to Owner about agreed provider;//silent push
              _.forEach(devices, function (device) {
                let options = {
                  text: "Provider was agreed on prolong session",
                  token: device.token,
                  data: {
                    providerId: sessionObj.providerId,
                    ownerId: sessionObj.ownerId,
                    sessionId: sessionObj.id
                  }
                };

                Queue.create('deliveryNotificationWorker', options).save();
              });
              return [];
            });
        }
      });
    });
  }
}

class SessionCancel {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.sessionModel = createSessionModel(app);
    this.usersModel = createUsersModel(app);
    this.billingsModel = createBillingsModel(app);
    this.devicesModel = createDevicesModel(app);
    this.billingsSessionsModel = createBillingsSessionsModel(app);
    this.sequelize = app.get('sequelizeClient');
  }

  patch(id, data, params) {
    const stripeService = this.app.get('stripeService');
    const {payload} = params;
    const {userId} = payload;
    let userObj = null;
    let accountType = 'provider';
    let sessionObj = null;
    let billingObj = null;
    let receiverUserId = null;

      return this.sessionModel
        .findById(id)
        .then((session) => {
          if (session === null) {
            return Promise.reject(new errors.BadRequest('Invalid session Id', {errors: {id: id}}, 400));
          }

          sessionObj = session;
          const {ownerId, providerId} = session;
          if (userId === ownerId) {
            accountType = 'owner';
          }

          return this.sequelize.transaction((_transaction) => {
            return this.sessionModel.update({
              isCancelled: true,
            }, {
              where: {
                id: id
              },
              transaction: _transaction
            }).then(result => {
              let types = [
                this.constants.BILLING_TYPES.SESSION_OWNER_PAYMENT,
                this.constants.BILLING_TYPES.PROLONG_SESSION_OWNER_PAYMENT
              ];
              return this.billingsModel.getLastBillingBySessionId(sessionObj.id, types);
            }).then(billing => {
              //todo - fine for owner if he cancelled(if provider - need look specification)

              receiverUserId = sessionObj.ownerId;
              if (accountType === 'owner') {
                receiverUserId = sessionObj.providerId;
              }

              let type = this.constants.BILLING_TYPES.SESSION_REFUND_TO_OWNER;
              //todo - penalty amount need to change(now we get 10 percents)
              let refundAmount = billing.amount;
              if (accountType === 'owner') {
                let percent = 10;
                refundAmount = billing.amount - ((billing.amount / 100) * percent);
              }
              let chargeId = billing.chargeId;
              return stripeService.refundCharge(refundAmount, chargeId)
                .then(result => {
                  if (accountType === 'owner') {
                    return this.billingsModel
                      .createRefundCharge(refundAmount, userId, type, chargeId);
                  }
                });
            }).catch(err => {
              _transaction.rollback(err);

              //logger.payments('payments', util.format('%s', 'stripe - problem with refunding charge :', {transferData: transferData, err: err}));
              return Promise.reject(new errors.BadRequest('stripe - problem with refunding charge: ', {errors: err}, 400));
            });
          });
        }).then((result) => {
            if (accountType === 'owner') {
              return this.billingsSessionsModel.connectBillingToSession(result.id, sessionObj.id);
            }
        }).then(() => {
            return this.devicesModel
              .getUserDevicesById(receiverUserId);
        }).then(devices => {
          //send push notification provider or owner that session was cancelled
          _.forEach(devices, function (device) {
            let options = {
              title: "Session was cancelled",
              text: "Session was cancelled",
              token: device.token,
              data: {
                providerId: sessionObj.providerId,
                ownerId: sessionObj.ownerId,
                sessionId: sessionObj.id
              }
            };

            Queue.create('deliveryNotificationWorker', options).save();
          });

          return [];
        });
  }
}

class SessionAmount {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.sessionModel = createSessionModel(app);
    this.sessionDetailsModel = createSessionDetailsModel(app);
  }

  create(data, params) {
    const {payload} = params;
    const {startDateTime, endDateTime, timezone} = data;
    const {userId} = payload;
    const constants = this.constants;

    return new Promise(function(resolve, reject) {

      /*example data
      let startDateTime = 1501329600000;
      let endDateTime = 1501369200000;
      let timezone = 'America/Los_Angeles';
      let timezone = 'Africa/Monrovia';*/

      let timezoneFormat = 'Z';
      let formattedTimezone = moment.tz(timezone).format(timezoneFormat);

      let dateWithoutTimeFormat = 'YYYY-MM-DD';
      let format = 'YYYY-MM-DD HH:mm:ss.ms';
      let localStartDateTime = moment(startDateTime).utcOffset(formattedTimezone).format(format);
      let localEndDateTime = moment(endDateTime).utcOffset(formattedTimezone).format(format);

      let countWalkSeconds = moment(localEndDateTime).diff(localStartDateTime, 'seconds');
      let localStartDate = moment(localStartDateTime).format(dateWithoutTimeFormat);
      let localEndDate = moment(localEndDateTime).format(dateWithoutTimeFormat);
      let diffDates = moment(localEndDate).diff(localStartDate, 'seconds');
      let localEndDateTimeHours = moment(localEndDateTime).hours();
      let localStartDateTimeHours = moment(localStartDateTime).hours();
      let controlDateTime = null;
      let amount = constants.MIN_AMOUNT;
      let amountOneMinute = constants.AMOUNT_ONE_MINUTE;
      let serviceAmount = constants.SERVICE_AMOUNT;
      let countSecondsBaseFund = 30 * 60;

      if (localStartDateTimeHours < 21 && (localEndDateTimeHours >= 21 || diffDates > 0)) {
        controlDateTime = localStartDate + " 21:00:00";
        let diffSecondsBeforeControlTime = moment(controlDateTime).diff(localStartDateTime, 'seconds');
        let diffSecondsAfterControlTime = moment(localEndDateTime).diff(controlDateTime, 'seconds');

        diffSecondsBeforeControlTime = diffSecondsBeforeControlTime - countSecondsBaseFund;
        amount = amount + (diffSecondsBeforeControlTime * 60 * amountOneMinute);

        if (amount > 25) {
          amount = 25;
        }

        diffSecondsAfterControlTime = diffSecondsAfterControlTime - countSecondsBaseFund;
        amount = amount + ((diffSecondsAfterControlTime / 60) * amountOneMinute);

        if (amount > 30) {
          amount = 30;
        }
      }

      if (localStartDateTimeHours >= 21) {
        countWalkSeconds = countWalkSeconds - countSecondsBaseFund;
        amount = amount + ((countWalkSeconds / 60) * amountOneMinute);

        if (amount > 30) {
          amount = 30;
        }
      }

      if (localStartDateTimeHours < 21 && (localEndDateTimeHours < 21 && !diffDates)) {
        countWalkSeconds = countWalkSeconds - countSecondsBaseFund;
        amount = amount + ((countWalkSeconds / 60) * amountOneMinute);

        if (amount > 25) {
          amount = 25;
        }
      }

      amount = amount + serviceAmount;

      return resolve({amount: amount});
    });
  }
}

class SessionBillings {
  setup(app) {
    this.app = app;
    this.constants = this.app.get('constants');
    this.sessionModel = createSessionModel(app);
    this.billingsModel = createBillingsModel(app);
    this.sessionDetailsModel = createSessionDetailsModel(app);
  }

  find(params) {
    const {SEARCH} = this.app.get('constants');
    const {query, session_id} = params;
    const {limit, page, offset} = query;

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    return this.billingsModel
      .getBillingsBySessionId(session_id, _limit, _offset)
      .then(billings => {
        if (billings == null) {
          return [];
        }
        return billings;
      });
  }
}



class SessionReview {
  setup(app) {
    this.app = app;
    this.sessionModel = createSessionModel(app);
    this.sessionDetailsModel = createSessionDetailsModel(app);
  }

  create(data, params) {
    const {payload, session_id} = params;
    const {comment, rating} = data;
    const {userId} = payload;
    var accountType = 'provider';

    return this.sessionModel.findById(session_id).then((session) => {
      if (session === null) {
        return new errors.BadRequest('Invalid session Id', {errors: {id: session_id}}, 400);
      }
      if (session.endDateTime === null) {
        return new errors.BadRequest('Invalid session "end date time" field', {}, 400);
      }

      let now = new Date();
      let endSession = new Date(session.endDateTime);
      if (endSession >= now) {
        return Promise.reject(new errors.BadRequest('Invalid session "end date time" field', {
          endDateTime: endSession,
          now: now
        }, 400));
      }

      const {ownerId, providerId} = session;
      if (userId === ownerId) {
        accountType = 'owner';
      } else if (userId !== providerId) {
        return Promise.reject(new errors.BadRequest('Invalid current user', {errors: {userId: userId}}, 400));
      }

      let options = {
        where: {
          sessionId: session_id,
          userId: userId
        }
      };

      return this.sessionDetailsModel.find(options);
    }).then((review) => {
      if (review) {
        return Promise.reject(new errors.BadRequest('Review already exist', {errors: {reviewId: review.id}}, 400));
      }

      let options = {
        sessionId: session_id,
        userId: userId,
        comment: comment,
        rating: parseInt(rating, 10),
        accountType: accountType,
        raw: true
      };

      return this.sessionDetailsModel.create(options).then(result => {
        return [];
      });
    });
  }
}

class SessionMessages {
  setup(app) {
    this.app = app;
    this.sessionMessagesModel = createSessionMessagesModel(app);
  }

  find(params) {
    const {SEARCH} = this.app.get('constants');
    const {query, session_id} = params;
    const {limit, page, offset} = query;

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    return this.sessionMessagesModel.findAll({
      where: {session_id: session_id},
      limit: _limit,
      offset: _offset
    }).then(messages => {
      if (messages == null) {
        return [];
      }
      return messages;
    });
  }
}


class SessionByTypes {
  setup(app) {
    this.app = app;
  }

  find(params) {

    const {payload, type, query} = params;
    const {limit, page, offset} = query;

    if (type !== 'upcoming' && type !== 'past') {
      return Promise.reject(new errors.BadRequest('Invalid session type', {errors: {type: type}}, 400));
    }

    const {SEARCH} = this.app.get('constants');
    const sequelize = this.app.get('sequelizeClient');

    const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
    const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
    const _offset = parseInt(offset, 10) || _page * _limit;

    let filters = 'AND';
    var result = [];


    if (type === 'upcoming') {
      filters += `
          "session".start_date_time > NOW()
          AND "session".is_cancelled = false
          ORDER BY "session".start_date_time ASC
        `;
    }

    if (type === 'past') {
      filters += `
          "session".end_date_time < NOW()
          ORDER BY "session".end_date_time DESC
        `;
    }

    let options = {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        ownerId: payload.userId,
        limit: _limit,
        offset: _offset
      }
    };

    return sequelize.query(`
          SELECT 
            "session".id AS id,
            "sessionDetail".rating AS "rating",
            "session".is_cancelled AS "isCancelled",

            "owner".name      AS "owner",
            "owner".image_url AS "ownerAvatar",

            "provider".name      AS "provider",
            "provider".image_url AS "providerAvatar",
            
            "session".start_date_time AS "start",
            "session".end_date_time   AS "end",

            "session".start_point AS "sessionAddress",
            "owner".address       AS "ownerAddress",
            "provider".address    AS "providerAddress",

            "owner".latitude  AS "ownerLatitude",
            "owner".longitude AS "ownerLongitude",

            "provider".latitude  AS "providerLatitude",
            "provider".longitude AS "providerLongitude",

            "session".latitude  AS "sessionLatitude",
            "session".longitude AS "sessionlongitude"

          FROM sessions "session"
          INNER JOIN users "provider" ON "provider".id = "session".provider_id
          INNER JOIN users "owner"  ON "owner".id = :ownerId
          LEFT OUTER JOIN session_details "sessionDetail" ON "sessionDetail".session_id = "session".id
          WHERE owner_id = :ownerId 
        ` + filters + `
          LIMIT  :limit
          OFFSET :offset;
        `, options)
      .then((sessions) => {

        result = sessions;
        return Promise.mapSeries(sessions, (session) => {

          let options = {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
              ownerId: payload.userId,
              sessionId: session.id
            }
          };

          return sequelize.query(`
            SELECT 
              "pet".id AS "id",
              "pet".name AS "name",
              "pet".image_url AS "avatar"
            FROM pets "pet"
            INNER JOIN pets_sessions "petSession" ON  "petSession".pet_id = "pet".id AND "petSession".session_id = :sessionId
            WHERE "pet".user_id = :ownerId 
          `, options);
        });
      }).then((sessionsPets) => {
        return Promise.mapSeries(sessionsPets, (sessionPets, index) => {
          return Object.assign({
            pets: sessionPets
          }, result[index]);
        });
      });
  }
}

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');
  const routePrefix = app.get('prefix');
  const routes = {
    basic: path.join(routePrefix, 'sessions'),
    session_provider: path.join(routePrefix, 'sessions/providers'),
    prolong_status: path.join(routePrefix, 'sessions/:session_id/prolong/status'),
    prolong: path.join(routePrefix, 'sessions/prolong'),
    review: path.join(routePrefix, 'sessions/:session_id/review'),
    type: path.join(routePrefix, 'sessions/type/:type'),
    session_messages: path.join(routePrefix, 'sessions/:session_id/messages'),
    session_billings: path.join(routePrefix, 'sessions/:session_id/billings'),
    session_cancel: path.join(routePrefix, 'sessions/cancel/'),
    session_amount: path.join(routePrefix, 'sessions/amount'),
  };

  app.use(path.join('/', routes['session_cancel']), new SessionCancel());
  const sessionCancelService = app.service(routes['session_cancel']);
  sessionCancelService.hooks(hooks);

  app.use(path.join('/', routes['prolong_status']), new SessionProlongStatus());
  const sessionProlongStatusService = app.service(routes['prolong_status']);
  sessionProlongStatusService.hooks(hooks);

  app.use(path.join('/', routes['prolong']), new SessionProlong());
  const sessionProlongService = app.service(routes['prolong']);
  sessionProlongService.hooks(hooks);

  app.use(path.join('/', routes['review']), new SessionReview());
  const sessionReviewService = app.service(routes['review']);
  sessionReviewService.hooks(hooks);

  app.use(path.join('/', routes['type']), new SessionByTypes());
  const sessionsByTypeService = app.service(routes['type']);
  sessionsByTypeService.hooks(hooks);

  app.use(path.join('/', routes['session_messages']), new SessionMessages());
  const sessionMessagesService = app.service(routes['session_messages']);
  sessionMessagesService.hooks(hooks);

  app.use(path.join('/', routes['session_provider']), new SessionProviders());
  const sessionProvidersService = app.service(routes['session_provider']);
  sessionProvidersService.hooks(hooks);

  app.use(path.join('/', routes['session_amount']), new SessionAmount());
  const SessionAmountService = app.service(routes['session_amount']);
  SessionAmountService.hooks(hooks);

  app.use(path.join('/', routes['session_billings']), new SessionBillings());
  const SessionBillingsService = app.service(routes['session_billings']);
  SessionBillingsService.hooks(hooks);

  const Model = createSessionModel(app);
  const options = {
    name: 'sessions',
    Model,
    paginate
  };

  app.use(path.join('/', routes['basic']), new Session());
  const service = app.service(routes['basic']);
  service.hooks(hooks);
};
