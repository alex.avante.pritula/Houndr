// Initializes the `pets-temp-sessions` service on path `/pets-temp-sessions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/pets-temp-sessions.model');
const hooks = require('./pets-temp-sessions.hooks');
const filters = require('./pets-temp-sessions.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'pets-temp-sessions',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/pets-temp-sessions', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('pets-temp-sessions');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
