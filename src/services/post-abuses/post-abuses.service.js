const createModel = require('../../models/post-abuses.model');
const hooks = require('./post-abuses.hooks');
const filters = require('./post-abuses.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const { SEARCH } = app.get('constants');

  class PostAbuses {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { query } = params;
      const { limit, page, offset } = query;

      const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
      const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10)   || _page * _limit;

      return Model.findAll({
        limit: _limit,
        offset: _offset,
        raw: true
      }).then(postAbuses => {
        if (postAbuses == null) {
          return [];
        }
        return postAbuses;
      });
    }

    create(data, params) {
      let jwtData = params.payload;
      return Model.create({
        text: data.text,
        postId: data.post_id,
        userId: jwtData.userId,
        raw: true
      });
    }
  }

  app.use('/api/v1/posts/abuses', new PostAbuses());

  const service = app.service('api/v1/posts/abuses');

  //get abuses for one post
  app.use('/api/v1/posts/:post_id/abuses', {
    find(params) {
      return Model.findAll({
        where: {post_id: params.post_id}
      }).then(abuses => {
        if (abuses == null) {
          return {};
        }
        return abuses;
      });
    }
  });

  const abusesCertainPostService = app.service('api/v1/posts/:post_id/abuses');

  service.hooks(hooks);
  abusesCertainPostService.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
