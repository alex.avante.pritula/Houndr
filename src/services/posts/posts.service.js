const path = require('path');
const hooks = require('./posts.hooks');
const createModel = require('../../models/posts.model');
const createPostImagesModel = require('../../models/post-images.model');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const ImagesModel = createPostImagesModel(app);
  const sequelize = app.get('sequelizeClient');
  const s3Service = app.get('s3Service');
  const routePrefix = app.get('prefix');
  const routes = {
    basic: path.join(routePrefix, 'posts'),
    search: path.join(routePrefix, 'posts/search'),
    attachedImages: path.join(routePrefix, 'posts/images/attached')
  };

  const {SEARCH} = app.get('constants');

  class Posts {
    setup(app) {
      this.app = app;
    }

    get(id) {
      return Model.getPost(id);
    }

    find(params) {

      const {post_code} = params.user;
      const {payload, query} = params;
      const {userId} = params.payload;
      const {limit, page, offset, new_comments} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      let where = {post_code: post_code};
      if (new_comments === 'count') {
        return Model.getPostCount(userId)
          .then(count => {
            return {count:count};
          });
      }
      if (new_comments === 'posts') {
        where = {
          user_id: userId,
          new_comments: true
        };
        return Model.getPosts(_limit, _offset, payload.userId, where);
      }

      return Model.getPosts(_limit, _offset, payload.userId, where);
    }

    create(data, params) {
      const {post_code} = params.user;
      const {userId} = params.payload;
      const {title, text, mainImage, attachedImages} = data;

      let options = {
        userId: userId,
        title: title,
        text: text,
        post_code: post_code || '',
        imageUrl: mainImage || ''
      };

      var transaction = null;
      var postId = null;
      var imagesArray = attachedImages || [];

      return sequelize.transaction((_transaction) => {
        return Model.create(options, {
          row: true,
          transaction: _transaction
        });
      }).then((post) => {
        postId = post.id;
        let images = imagesArray.map((imageUrl) => {
          return {
            postId: postId,
            url: imageUrl
          };
        });
        return ImagesModel.bulkCreate(images, {
          row: true,
          transaction: transaction
        });
      }).then(() => {
        return Model.getPost(postId);
      });
    }
  }

  class AttachedPostImages {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {files, id} = params;

      return Model.findById(id).then(() => {
        return s3Service.uploadImages('posts', files);
      });
    }
  }

  class PostSearch {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const {payload, query} = params;
      const {limit, page, offset} = query;
      let {title, text} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      title = (typeof title !== 'undefined') ? title : '';
      text = (typeof text !== 'undefined') ? text : '';

      return Model.searchPosts(title, text, payload.userId, _limit, _offset);
    }
  }

  app.use('/' + routes['search'], new PostSearch());
  const postSearchService = app.service(routes['search']);

  app.use('/' + routes['basic'], new Posts());
  const service = app.service(routes['basic']);

  app.use('/' + routes['attachedImages'], new AttachedPostImages());
  const attachedPostImagesService = app.service(routes['attachedImages']);

  service.hooks(hooks);
  postSearchService.hooks(hooks);
  attachedPostImagesService.hooks(hooks);
};
