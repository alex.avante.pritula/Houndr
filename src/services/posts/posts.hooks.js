const { authenticate } = require('feathers-authentication').hooks;
const errors = require('feathers-errors');
const { getErrorObject } = require('../../errorHandler');
const createPostCommentModel = require('../../models/post-comments.model');
const createPostLikeModel = require('../../models/post-likes.model');
const validators = require('../../utils/validators');
//const formatters = require('../../utils/formatters');

function validate(hook) {
  let validateErrors = '';
  if (hook.method === 'create' && hook.path === 'api/v1/posts') {
    validateErrors = validators.post.create(hook.data);
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }

  return hook;
}

function formateResponse(hook) {
  /*if (hook.method === 'get' && hook.path === 'api/v1/posts') {
    hook.result = formatters.post.getPost(hook.result);
  }*/

  /*if (hook.method === 'find' && hook.path === 'api/v1/posts') {
    hook.result = formatters.post.getPosts(hook.result);
  }*/

  return hook;
}

function modifyPost(hook) {
  if (hook.method === 'get' && hook.path === 'api/v1/posts') {
    const userId = hook.params.payload.userId;
    let postCommentModel = createPostCommentModel(hook.app);
    let postLikeModel = createPostLikeModel(hook.app);
    let post = hook.result;
    if (post === null) return hook;

    return postCommentModel.count({
      where: {post_id: post.id}
    }).then(commentsCount => {
      hook.result.dataValues['comment_count'] = commentsCount;
      return postLikeModel.count({
        where: {post_id: post.id}
      });
    }).then(likesCount => {
      hook.result.dataValues['like_count'] = likesCount;
      return postLikeModel.findOne({
        where: {
          post_id: post.id,
          user_id: userId,
        }
      });
    }).then(like => {
      hook.result.dataValues['is_liked'] = true;

      if (like === null) {
        hook.result.dataValues['is_liked'] = false;
      }

      return hook;
    });
  }
}

module.exports = {
  before: {
    all: [ authenticate('jwt'), validate ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [formateResponse],
    find: [],
    get: [modifyPost],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
