const createModel = require('../../models/post-likes.model');
const hooks = require('./post-likes.hooks');
const filters = require('./post-likes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
 
  const { SEARCH } = app.get('constants');

  class PostLikes {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const {  query } = params;
      const { limit, page, offset } = query;

      const _limit  = parseInt(limit, 10)    || SEARCH['LIMIT'];
      const _page   = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10)   || _page * _limit;

      return Model.findAll({
        limit: _limit,
        offset: _offset,
        raw: true
      }).then(postLikes => {
        if (postLikes == null) {
          return {};
        }
        return postLikes;
      });
    }

    create(data, params) {
      let jwtData = params.payload;
      return Model.create({
        postId: data.post_id,
        userId: jwtData.userId,
        raw: true
      });
    }

    remove(id, params) {
      let jwtData = params.payload;
      return Model.destroy({
        where: {
          id: id,
          user_id: jwtData.userId
        }
      });
    }
  }

  app.use('/api/v1/posts/likes', new PostLikes());
  const service = app.service('api/v1/posts/likes');


  //get likes by postId
  app.use('/api/v1/posts/:post_id/likes', {
    find(params) {
      return Model.findAll({
        where: {post_id: params.post_id}
      }).then(results => {
        if (results == null) {
          return {};
        }
        return results;
      });
    }
  });

  //get likes by postId and userId
  app.use('/api/v1/posts/:post_id/users/:user_id/likes', {
    find(params) {
      return Model.findOne({
        where: {
          post_id: params.post_id,
          user_id: params.user_id
        }
      }).then(result => {
        if (result == null) {
          return {};
        }
        return result;
      });
    }
  });

  //get likes by userId
  app.use('/api/v1/users/:user_id/likes', {
    find(params) {
      return Model.findAll({
        where: {
          user_id: params.user_id
        }
      }).then(results => {
        if (results == null) {
          return {};
        }
        return results;
      });
    }
  });

  const postLikeService = app.service('api/v1/posts/:post_id/likes');
  const postUserLikeService = app.service('api/v1/posts/:post_id/users/:user_id/likes');
  const userLikeService = app.service('api/v1/users/:user_id/likes');

  service.hooks(hooks);
  postLikeService.hooks(hooks);
  postUserLikeService.hooks(hooks);
  userLikeService.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
