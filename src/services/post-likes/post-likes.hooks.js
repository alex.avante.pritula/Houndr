const {authenticate} = require('feathers-authentication').hooks;
const {getErrorObject} = require('../../errorHandler');
const errors = require('feathers-errors');
const _ = require('lodash');
const createPostLikeModel = require('../../models/post-likes.model');
const createPostModel = require('../../models/posts.model');
const createDevicesModel = require('../../models/devices.model');
const validators = require('../../utils/validators');
const formatters = require('../../utils/formatters');
const kue = require('kue');
const Queue = kue.createQueue();

function validate(hook) {
  let validateErrors = '';
  if (hook.method === 'create' && hook.path === 'api/v1/posts/likes') {
    let postLikeModel = createPostLikeModel(hook.app);
    const userId = hook.params.payload.userId;

    return postLikeModel.findOne({
      where: {
        post_id: hook.data.post_id,
        user_id: userId,
      }
    }).then(result => {
      if (result != null) {
        validateErrors = 'User has already liked this post';
        return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
      }
    });
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }

  return hook;
}

function sendNotifications(hook) {
  if (hook.method === 'create' && hook.path === 'api/v1/posts/likes') {
    const userId = hook.params.payload.userId;
    const postId = hook.data.post_id;
    const devicesModel = createDevicesModel(hook.app);
    const postsModel = createPostModel(hook.app);

    postsModel
      .getPostOwner(postId)
      .then(post => {
        return devicesModel.getUserDevicesById(post.userId);
      }).then(devices => {
      if (devices == null) {
        return hook;
      }

      let userTokens = [];
      _.forEach(devices, function (device) {
        let options = {
          title: "user liked your post",
          text: "user liked your post",
          token: device.token,
          data: {
            userFrom: {
              userId: userId
            }
          }
        };

        //todo - uncomment when will be prepared notifications
        //Queue.create('deliveryNotificationWorker', options).save();
      });

      return hook;
    });
  }
}

module.exports = {
  before: {
    all: [authenticate('jwt'), validate],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [sendNotifications],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
