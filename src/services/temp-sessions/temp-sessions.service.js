const path = require('path');
const _ = require('lodash');
const errors = require('feathers-errors');
const kue = require('kue');
const moment = require('moment');
const momentTimezone = require('moment-timezone');

const hooks = require('./temp-sessions.hooks');
const filters = require('./temp-sessions.filters');

const createUserModel = require('../../models/users.model');
const createTempSessionsModel = require('../../models/temp-sessions.model');
const createTempSessionProvidersModel = require('../../models/temp-session-providers.model');
const createPetsTempSessionsModel = require('../../models/pets-temp-sessions.model');
const createSessionsModel = require('../../models/sessions.model');
const createPetsSessionsModel = require('../../models/pets-sessions.model');
const createPetsModel = require('../../models/pets.model');
const createDevicesModel = require('../../models/devices.model');
const Queue = kue.createQueue();

module.exports = function () {
  const app = this;
  const {SEARCH, TIMEOUT /*RADUIS, LONGITUDE, LATITUDE */} = app.get('constants');

  const routePrefix = app.get('prefix');
  const route = {
    basic: path.join(routePrefix, 'temp-sessions'),
    providers: path.join(routePrefix, 'temp-sessions/:sessionId/providers/'),
    neighbors: path.join(routePrefix, 'temp-sessions/:sessionId/neighbors/'),
    pets: path.join(routePrefix, 'temp-sessions/:sessionId/pets'),
  };

  const sequelize = app.get('sequelizeClient');

  const Model = createTempSessionsModel(app);
  const UserModel = createUserModel(app);
  const ProviderModel = createTempSessionProvidersModel(app);
  const PetsTempSessionsModel = createPetsTempSessionsModel(app);
  const SessionsModel = createSessionsModel(app);
  const PetsSessionsModel = createPetsSessionsModel(app);
  const devicesModel = createDevicesModel(app);
  const petsModel = createPetsModel(app);

  class TempSessionPets {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {sessionId, payload} = params;
      const {userId} = payload;
      const {pets} = data;

      let petsTempSession = pets.map((petId) => {
        return {
          tempSessionId: sessionId,
          petId: petId
        };
      });

      return Model.findOne({
        where: {
          id: sessionId,
          owner_id: userId,
        }
      }).then((session) => {
        if (session === null) {
          return Promise.reject(new errors.BadRequest('Invalid session Id', {errors: {id: sessionId}}, 400));
        }
      }).then((userPets) => {
        return petsModel.findAndCountAll({
          where: {
            id: pets,
            user_id: userId,
          }
        });
      }).then((userPets) => {
        if (userPets.count != pets.length) {
          return Promise.reject(new errors.BadRequest('Some pets are not belong to current user', {errors: {id: userId}}, 400));
        }

        return sequelize.transaction((_transaction) => {
          return PetsTempSessionsModel.bulkCreate(petsTempSession, {
            transaction: _transaction
          });
        });
      });
    }
  }

  class TempSession {
    setup(app) {
      this.app = app;
    }

    get(id) {
      return Model.getTempSessionById(id).then(tempSessions => {
        if (tempSessions === null) {
          return [];
        }
        return tempSessions;
      });
    }

    create(data, params) {
      const {userId} = params.payload;
      const {startDateTime, endDateTime, timezone, longitude, latitude} = data;

      //example
      //let timezone = 'America/Los_Angeles';
      let timezoneFormat = 'Z';
      let calcTimezone = momentTimezone.tz(timezone).format(timezoneFormat);

      let format = 'YYYY-MM-DD HH:mm:ss.ms';
      let startCalcDateTime = moment.utc(startDateTime).format(format);
      let endCalcDateTime = moment.utc(endDateTime).format(format);

      if (calcTimezone !== '+00.00') {
        startCalcDateTime = startCalcDateTime + ' ' + calcTimezone;
        endCalcDateTime = endCalcDateTime + ' ' + calcTimezone;
      }

      let options = {
        ownerId: userId,
        endDateTime: startCalcDateTime,
        startDateTime: endCalcDateTime,
        longitude: longitude,
        latitude: latitude
      };

      return Model.create(options);
    }

    remove(id) {
      let options = {where: {id: id}};
      return Model.destroy(options);
    }

  }

  class TempSessionProviders {
    setup(app) {
      this.app = app;
      this.usersModel = createUserModel(app);
    }

    find(params) {
      const {query, sessionId} = params;
      const {limit, page, offset, status} = query;

      const _limit = parseInt(limit, 10) || (SEARCH['LIMIT'] / 2);
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      let statusValue = status || 'agreed';

      return ProviderModel
        .getTempSessionByIdAndStatus(sessionId, statusValue, limit, offset);
    }

    patch(id, data, params) {
      let status = data.status || 'rejected';
      const {userId} = params.payload;
      const {sessionId} = params;

      let tempProviderObj = null;
      let userObj = null;

      return this.usersModel
        .getUserById(userId)
        .then((user) => {
          userObj = user;

          if (!userObj.stripeAccountId.length) {
            return Promise.reject(new errors.BadRequest('User does not have stripe account', {errors: {}}, 400));
          }

          return ProviderModel
            .getProviderInTempSession(sessionId, userId);
        }).then((tempProvider) => {
          if (!tempProvider) {
            return Promise.reject(new errors.BadRequest('Invalid temp session Id or provider id', {errors: {id: userId}}, 400));
          }

          let range = (new Date() - new Date(tempProvider.createdAt));
          if (status === 'agreed' && range > TIMEOUT) {
            return Promise.reject(new errors.BadRequest('Invalid range', {errors: {range: range}}, 400));
          }

          tempProviderObj = tempProvider;

          return ProviderModel.getCountSessionsByRangeDate(
            tempProvider.temp_sessions.startDateTime,
            tempProvider.temp_sessions.endDateTime,
            userId,
            tempProvider.tempSessionId
          );

        }).then(sessions => {
          if (sessions[0].count > 0) {
            return Promise.reject(new errors.BadRequest('You have sessions in same time', {errors: {}}, 400));
          }

          tempProviderObj.status = status;

          if (status === 'agreed') {
            return tempProviderObj.save()
              .then((result) => {
                return devicesModel.getUserDevicesById(tempProviderObj.temp_sessions.ownerId);
              }).then((devices) => {
                // TODO => check silent push
                //Send PN to Owner about agreed provider;//silent push
                _.forEach(devices, function (device) {
                  let options = {
                    text: "Provider was agreed",
                    token: device.token,
                    data: {
                      providerId: tempProviderObj.providerId,
                      tempSessionId: tempProviderObj.temp_sessions.id
                    }
                  };

                  Queue.create('deliveryNotificationWorker', options).save();
                });
                return [];
              });
          }

          return [];
        });
    }

    create(data, params) {
      const {sessionId} = params;
      const {status, providerId} = data;
      let options = {
        tempSessionId: sessionId,
        providerId: providerId,
        status: status || 'agreed'
      };
      return ProviderModel.create(options);
    }
  }

  class Neighbors {

    setup(app) {
      this.app = app;
    }

    find(params) {
      const {payload, sessionId} = params;
      const {limit, offset, page, radius, isRestarted} = params.query;
      const _limit = parseInt(limit, 10) || (SEARCH['LIMIT'] / 2);
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      var currentUser = {};
      let countProviders = 0;
      let tempSessionObj = null;
      let preparedRadius = (typeof(radius) === 'undefined') ? 1 : radius;

      return UserModel.findById(payload.userId)
        .then((user) => {

          currentUser = user;

          if (isRestarted) {
            let options = {
              where: {
                tempSessionId: sessionId /*,
                 status: { ne: 'await'} */
              }
            };
            return ProviderModel.destroy(options);
          }

        }).then(() => {
          return Model.findById(sessionId);
        }).then((tempSession) => {
          const {startDateTime, endDateTime} = tempSession;
          const {latitude, longitude, address,  id} = currentUser;

          tempSessionObj = tempSession;
          //todo - check need or don't need to multiply date(timestamp) on 1000
          let options = {
            end: endDateTime,
            start: startDateTime,
            latitude: latitude,
            longitude: longitude,
            ownerId: id,
            radius: preparedRadius,
            address: address,
            limit: _limit,
            offset: _offset
          };

          return UserModel.getNeighbors(options);
        }).then((providers) => {
          let items = providers.map((provider) => {
            return {
              tempSessionId: sessionId,
              providerId: provider.id,
              status: 'await'
            };
          });

          return ProviderModel.bulkCreate(items);
        }).then((providers) => {
          //send PN to free providers;
          let ids = [];
          _.forEach(providers, function (provider) {
            ids.push(provider.id);
          });

          countProviders = providers.length;
          return devicesModel.getUserDevicesById(ids);
        }).then((devices) => {

          _.forEach(devices, function (device) {
            let options = {
              title: "You were invited to walk the dog",
              text: "You were invited to walk the dog",
              token: device.token,
              data: {
                ownerId: tempSessionObj.ownerId,
                id: tempSessionObj.id
              }
            };

            Queue.create('deliveryNotificationWorker', options).save();
          });

          return {count: countProviders};
        });
    }
  }

  app.use(path.join('/', route['basic']), new TempSession());
  const service = app.service(route['basic']);
  service.hooks(hooks);

  app.use(path.join('/', route['providers']), new TempSessionProviders());
  const providerService = app.service(route['providers']);
  providerService.hooks(hooks);

  app.use(path.join('/', route['neighbors']), new Neighbors());
  const neighborService = app.service(route['neighbors']);
  neighborService.hooks(hooks);

  app.use(path.join('/', route['pets']), new TempSessionPets());
  const tempSessionPetsService = app.service(route['pets']);
  tempSessionPetsService.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }

  if (providerService.filter) {
    providerService.filter(filters);
  }

  if (neighborService.filter) {
    neighborService.filter(filters);
  }
};
