const { authenticate } = require('feathers-authentication').hooks;
const formatters = require('../../utils/formatters');
const path = require('path');
const { getErrorObject } = require('../../errorHandler');

module.exports = {
  before: {
    all: [
      authenticate('jwt')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      function validate(hook) {
        const rootPrefix = hook.app.get('prefix');
        if (hook.method === 'find' && hook.path === path.join(rootPrefix, '/temp-sessions/:sessionId/providers')) {
          hook.result = formatters.session.getNeighbors(hook.result);
        }
        return hook;
      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
