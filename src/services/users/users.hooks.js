const {authenticate} = require('feathers-authentication').hooks;
const commonHooks = require('feathers-hooks-common');
const {restrictToOwner} = require('feathers-authentication-hooks');

const {hashPassword} = require('feathers-authentication-local').hooks;
const userModelCreate = require('../../models/users.model');
const errors = require('feathers-errors');
const {getErrorObject} = require('../../errorHandler');
const validators = require('../../utils/validators');
const formatters = require('../../utils/formatters');

const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: 'id',
    ownerField: 'id'
  })
];

function verifyUserAccount(hook) {
  if (hook.data.facebook) {
    let userModel = userModelCreate(hook.app);
    let email = hook.data.facebook.profile._json.email;
    let name = hook.data.facebook.profile.displayName;

    userModel.update({
      email: email,
      name: name
    }, {
      where: {
        facebook_id: hook.data.facebook.profile.id
      }
    }).then(() => {
      return hook;
    });
  } else if (hook.data.google) {
    let userModel = userModelCreate(hook.app);
    let email = hook.data.google.profile.emails[0].value;
    let name = hook.data.google.profile.displayName;

    userModel.update({
      email: email,
      name: name
    }, {
      where: {
        google_id: hook.data.google.profile.id
      }
    }).then(() => {
      return hook;
    });
  } else if (hook.data.twitter) {
    let email = hook.data.twitter.profile.emails[0].value;
    let name = hook.data.twitter.profile.displayName;
    let userModel = userModelCreate(hook.app);

    userModel.update({
      email: email,
      name: name
    }, {
      where: {
        twitter_id: hook.data.twitter.profile.id
      }
    }).then(() => {
      return hook;
    });
  } else {
    return hook;
  }
}

function beforeVerifyUserAccount(hook) {
  let userModel = userModelCreate(hook.app);
  if (hook.data.facebook) {
    let email = hook.data.facebook.profile._json.email;
    getUserByEmail(userModel, email)
      .then(function (user) {
        if (user === null) {
          return hook;
        }

        userModel.update({
          email: email,
          facebookId: hook.data.facebook.profile.id,
          name: hook.data.facebook.profile.displayName,
        }, {
          where: {
            email: email
          }
        }).then(() => {
          hook.result = true;
          return hook;
        });
      });
  } else if (hook.data.google) {
    let email = hook.data.google.profile.emails[0].value;
    getUserByEmail(userModel, email)
      .then(function (user) {
        if (user === null) {
          return hook;
        }

        userModel.update({
          email: email,
          googleId: hook.data.google.profile.id,
          name: hook.data.google.profile.displayName,
        }, {
          where: {
            email: email
          }
        }).then(() => {
          hook.result = true;
          return hook;
        });
      });
  } else if (hook.data.twitter) {
    let email = hook.data.twitter.profile.emails[0].value;

    getUserByEmail(userModel, email)
      .then(function (user) {
        if (user === null) {
          return hook;
        }

        userModel.update({
          twitterId: hook.data.twitter.profile.id,
          name: hook.data.twitter.profile.displayName,
        }, {
          where: {
            email: email
          }
        }).then(() => {
          hook.result = true;
          return hook;
        });
      });
  } else {
    let email = hook.data.email;
    getUserByEmail(userModel, email)
      .then(function (user) {
        if (user === null) {
          return hook;
        }

        userModel.update({
          email: email,
          password: hook.data.password,
          name: hook.data.name
        }, {
          where: {
            email: email
          }
        }).then(() => {
          hook.result = true;
          return hook;
        });
      });
  }
}

function getUserByEmail(userModel, email) {
  return userModel
    .getUserByEmail(email);
}
/*
function beforeVerify(hook) {
  //console.log(hook.params);
  return hook;
}
*/
function checkRestrictionMethods(hook) {
  if ((hook.method == 'get' || hook.method == 'find') && hook.path == 'api/v1/users/email/:email') {
    return true;
  }

  if ((hook.method == 'patch') && hook.path == 'api/v1/users/avatar') {
    return true;
  }

  return false;
}

function checkAllowingAuthenticateCreateMethods(hook) {
  if (hook.method == 'create' && hook.path == 'api/v1/users/devices') {
    return true;
  }
  if (hook.method == 'create' && hook.path == 'api/v1/users/avatar') {
    return true;
  }

  return false;
}

function checkIsUserService(hook) {
  if (hook.path == 'users' || hook.path == 'api/v1/users') {
    return true;
  }

  return false;
}

function validate(hook) {
  let validateErrors = '';
  if (hook.method === 'create' && hook.path === 'api/v1/users') {
    validateErrors = validators.user.create(hook.data);
  }
  if (hook.method === 'update' && hook.path === 'api/v1/users') {
    validateErrors = validators.user.update(hook.data);
  }

  if (hook.method === 'patch' && hook.path === 'api/v1/users/accounts') {
    validateErrors = validators.user.updateAccount(hook.data);
  }

  if (hook.method === 'patch' && hook.path === 'api/v1/users/customers') {
    validateErrors = validators.user.updateCustomer(hook.data);
  }

  if (validateErrors.length > 0) {
    return Promise.reject(new errors.BadRequest('Error validation: ' + validateErrors, {errors: validateErrors}, 400));
  }

  return hook;
}

function formateResponse(hook) {
  if (hook.method === 'find' && hook.path === 'api/v1/users/current') {
    hook.result = formatters.user.getUser(hook.result);
  }

  if (hook.method === 'find' && hook.path === 'api/v1/users') {
    hook.result = formatters.user.getUsers(hook.result);
  }

  if (hook.method === 'create' && hook.path === 'api/v1/users') {
    hook.result = formatters.user.createUser(hook.result);
  }
  return hook;
}
function isRestorePassword(hook) {
  if (hook.method === 'update' && hook.path === 'api/v1/users/password') {
    return true;
  }
  return hook;
}

module.exports = {
  before: {
    all: [validate],
    find: [commonHooks.iffElse(checkRestrictionMethods, [], authenticate('jwt')),],
    get: [commonHooks.iffElse(checkRestrictionMethods, [], ...restrict)],
    create: [
      commonHooks.iffElse(checkIsUserService, [
        hashPassword(),
        beforeVerifyUserAccount
      ], [
        commonHooks.iffElse(checkAllowingAuthenticateCreateMethods, authenticate('jwt'), []),
      ])
    ],
    update: [commonHooks.iffElse(isRestorePassword, [authenticate('jwt')],  [hashPassword()])],
    patch: [...restrict, hashPassword()],
    remove: [...restrict]
  },

  after: {
    all: [
      commonHooks.when(
        hook => hook.params.provider,
        commonHooks.discard('password')
      ),
      formateResponse
    ],
    find: [],
    get: [],
    create: [
      commonHooks.iffElse(checkIsUserService, [
        verifyUserAccount
      ], [])
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        hook.error = getErrorObject(hook.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
