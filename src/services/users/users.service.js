const path = require('path');
const createService = require('feathers-sequelize');
const createModel = require('../../models/users.model');
const createPetsModel = require('../../models/pets.model');
const hooks = require('./users.hooks');
//const filters = require('./users.filters');
const errors = require('feathers-errors');
var crypto = require('crypto');


module.exports = function () {
  const app = this;

  const Model = createModel(app);
  const paginate = app.get('paginate');
  const {SEARCH} = app.get('constants');
  const s3Service = app.get('s3Service');
  const routePrefix = app.get('prefix');
  const authenticationConfig = app.get('authentication');
  const ExtractJwt = require('passport-jwt').ExtractJwt;

  const routes = {};

  const options = {
    name: 'users',
    Model,
    paginate
  };

  class Users {
    setup(app) {
      this.app = app;
    }

    get(id) {
      return Model.findOne({
        where: {id: id},
        attributes: [
          'email',
          ['name', 'username'],
          'age',
          'address',
          'longitude',
          'latitude',
          ['stripe_customer_id', 'stripe_customer_id'],
          ['is_provider', 'is_provider'],
          ['has_notifications', 'has_notifications']
        ]
      }).then(user => {
        if (user == null) {
          return {};
        }
        return user;
      });
    }

    remove(id, params) {
      let self = this;
      return new Promise(function (resolve, reject) {
        let accessToken = ExtractJwt.fromHeader('authorization')(params);
        return app.service('authentication').remove(accessToken).then(response => {
          return resolve([]);
        });
      });
    }

    find(params) {
      const {query} = params;
      const {limit, page, offset} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.findAll({
        limit: _limit,
        offset: _offset,
        raw: true
      }).then(user => {
        if (user == null) {
          return {};
        }
        return user;
      });
    }

    create(data) {
      return Model.create({
        email: data.email,
        password: data.password,
        name: data.name,
        last_name: data.lastName,
        post_code: data.postCode,
        is_have_dog: data.isHaveDog,
        raw: true
      }).then(user => {
        return this.app.passport.createJWT({
          userId: user.id,
          email: user.email,
          name: user.name,
        }, this.app.get('authentication'));
      }).then(accessToken => {
        return {accessToken: accessToken};
      });
    }

    update(id, data, params) {
      const {payload} = params;

      return Model.update(
        {
          name: data.name,
          age: data.age,
          address: data.address,
          longitude: data.longitude,
          latitude: data.latitude,
          isProvider: data.is_provider,
          hasNotifications: data.has_notifications,
          last_name: data.lastName,
          post_code: data.postCode,
          is_have_dog: data.isHaveDog,
          available_as_owner: data.available_as_owner,
          available_as_provider: data.available_as_provider,
          about_myself: data.about_myself,
          phone: data.phone,
        }, {
          where: {
            id: payload.userId
          }
        }).then(() => {
          return data;
        });
    }
  }

  class UserAccounts {
    setup(app) {
      this.app = app;
      this.usersModel = Model;
    }

    patch(id, data, params) {
      const {payload, country, currency} = params;
      const {userId} = payload;
      const stripeService = this.app.get('stripeService');
      let accountObj = null;

      /*example data
       let accountParams = {
       country: 'US',
       currency: 'usd',
       };*/

      let accountParams = {
        country: data.country,
        currency: data.currency,
      };

      return this.usersModel
        .getUserById(userId)
        .then(user => {
          if (!user.email) {
            return Promise.reject(new errors.BadRequest('user does not have email: ', {errors: err}, 400));
          }

          accountParams.email = user.email;
          return stripeService.createStripeVirtualAccount(accountParams);
        }).then(account => {
          accountObj = account;
          return this.usersModel
            .updateStripeAccountId(accountObj.id, userId);
        }).then(result => {
          return {
            accountId: accountObj.id
          };
        }).catch(err => {
          //logger.payments('payments', util.format('%s', 'stripe - problem with creating account :', {transferData: transferData, err: err}));
          return Promise.reject(new errors.BadRequest('stripe - problem with creating account: ', {errors: err}, 400));
        });
    }
  }

  class UserCustomers {
    setup(app) {
      this.app = app;
      this.usersModel = Model;
    }

    patch(id, data, params) {
      const {payload} = params;
      const {userId} = payload;
      const stripeService = this.app.get('stripeService');
      let customerObj = null;

      /*example data
       let card_number = 4242424242424242;
       let exp_month = 12;
       let exp_year = 2018;
       let cvv = 9999;*/

      return this.usersModel
        .getUserById(userId)
        .then(user => {
          if (!user.email) {
            return Promise.reject(new errors.BadRequest('user does not have email: ', {errors: err}, 400));
          }
          params.email = user.email;
          return stripeService.createCustomer(
            data.card_number,
            data.exp_month,
            data.exp_year,
            data.cvv
          );
        }).then(customer => {
          customerObj = customer;
          return this.usersModel
            .updateStripeCustomerId(customerObj.id, userId);
        }).then(result => {
          return {
            customerId: customerObj.id
          };
        }).catch(err => {
          //logger.payments('payments', util.format('%s', 'stripe - problem with creating account :', {transferData: transferData, err: err}));
          return Promise.reject(new errors.BadRequest('stripe - problem with creating customer: ', {errors: err}, 400));
        });
    }
  }

  app.use('/api/v1/users/email/:email', {
    find(params) {
      return Model.findOne({
        where: {email: params.email},
        attributes: ['email']
      }).then(user => {
        if (user == null) {
          return {};
        }
        return user;
      });
    }
  });

  app.use('/api/v1/users/current', {
    find(params) {
      let jwtData = params.payload;
      return Model.findOne({
        where: {id: jwtData.userId},
        raw: true
      }).then(user => {
        if (user == null) {
          return {};
        }
        return user;
      });
    }
  });

  app.use('/api/v1/users/password', {
    update(id, data, params) {

      let newToken;
      const usaerEmail = data.email;

      return Model.getUserByEmail(usaerEmail)
        .then(user => {
          if (user === null) {
            return Promise.reject('invalid email');
          }
          newToken = crypto.createHash('md5').update(`${user.id}${new Date()}`).digest('hex');
          return Model.updateRestoreTocen(user.id, newToken);
        }).then(() => {
          let email = {
            from: 'gethoundrjosh@gmail.com',
            to: usaerEmail,
            subject: 'Restore Password',
            html: `<p>To restore your password please follow the link:</p>
            <a href=http://${app.get('host')}:${app.get('port')}/api/v1/password/${newToken}>Link to password recovery</a>`

          };
          return app.service('/mailer')
            .create(email);
        }).then(() => {
          return {message: 'email was sent'};
        }).catch(err => {
          return Promise.reject(new errors.BadRequest('cannot restore password', {errors: err}, 400));
        });

    }
  });

  app.use('/api/v1/users/avatar', {
    patch(id, data, params) {
      const {userId} = params.payload;
      const {files} = params;

      var temp = {
        currentUser: null,
        s3Image: null
      };

      return Model.findById(userId)
        .then((user) => {
          if (!user) {
            return Promise.reject(new errors.BadRequest('Invalid user Id', {errors: {id: userId}}, 400));
          }

          temp.currentUser = user;
          return s3Service.uploadImage('users', files[0]);
        }).then((s3Image) => {
          temp.s3Image = s3Image;
          const {imageUrl} = temp.currentUser;
          if (!imageUrl) return;
          return s3Service.deleteImage('users', imageUrl);
        }).then(() => {
          let {currentUser, s3Image} = temp;
          currentUser.imageUrl = s3Image.url;
          return currentUser.save();
        }).then(() => {
          return temp.s3Image;
        });
    },

    create(data, params){
      const {userId} = params.payload;
      const {files} = params;

      var currentUser = null;

      return Model.findById(userId).then((user) => {
        if (!user) {
          return Promise.reject(new errors.BadRequest('Invalid user Id', {errors: {id: userId}}, 400));
        }

        currentUser = user;
        return s3Service.uploadImage('users', files[0]);
      }).then((s3Image) => {
        currentUser.imageUrl = s3Image.url;
        return currentUser.save();
      }).then(() => {
        return {
          url: currentUser.imageUrl
        };
      });
    }
  });

  app.use('/api/v1/users/accounts', new UserAccounts());
  app.use('/api/v1/users/customers', new UserCustomers());

  app.use('/users', createService(options));
  app.use('/api/v1/users', new Users());

  const service = app.service('users');
  const userService = app.service('api/v1/users');
  const currentUserService = app.service('api/v1/users/current');
  const userPasswordService = app.service('api/v1/users/password');
  const avatarUserService = app.service('api/v1/users/avatar');
  const userEmailService = app.service('api/v1/users/email/:email');
  const UserAccountsService = app.service('api/v1/users/accounts');
  const UserCustomersService = app.service('api/v1/users/customers');

  service.hooks(hooks);
  userService.hooks(hooks);
  currentUserService.hooks(hooks);
  userEmailService.hooks(hooks);
  userPasswordService.hooks(hooks);
  avatarUserService.hooks(hooks);
  UserAccountsService.hooks(hooks);
  UserCustomersService.hooks(hooks);
};

