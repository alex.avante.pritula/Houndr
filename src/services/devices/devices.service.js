const createModel = require('../../models/devices.model');
const hooks = require('./devices.hooks');
const filters = require('./devices.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);

  class Devices {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      let jwtData = params.payload;
      return Model.create({
        deviceId: data.device_id,
        token: data.token,
        userId: jwtData.userId,
        raw: true
      });
    }

    update(id, data, params) {
      let jwtData = params.payload;
      return Model.update({
        token: data.token,
      }, {
        where: {
          deviceId: data.device_id,
          userId: jwtData.userId
        }
      });
    }

    remove(id, params) {
      const { userId } = params.payload;
      const { device_id } = params.query;

      return Model.destroy({
        where: {
          deviceId: device_id,
          userId: userId
        }
      });
    }
  }

  app.use('/api/v1/users/devices', new Devices());

  const service = app.service('api/v1/users/devices');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
