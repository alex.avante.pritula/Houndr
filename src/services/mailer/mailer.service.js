


const hooks = require('./mailer.hooks');
const filters = require('./mailer.filters');
const mailer = require('feathers-mailer');
const smtpTransport = require('nodemailer-smtp-transport');

module.exports = function () {
  const app = this;
  const conf = app.get('mailer');
  let mailerTransportParams = {
    service: conf.service,
    host: conf.host,
    port: conf.port,
    auth: {
      user: conf.login,
      pass: conf.password
    },
    secure: conf.secure,
    tls: {
      rejectUnauthorized: false
    }
  };

  app.use('/mailer', mailer(smtpTransport(mailerTransportParams)));
  const service = app.service('mailer');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
