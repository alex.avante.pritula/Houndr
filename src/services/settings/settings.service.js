const createService = require('feathers-sequelize');
const createModel = require('../../models/settings.model');
const hooks = require('./settings.hooks');
const filters = require('./settings.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'settings',
    Model,
    paginate
  };

  app.use('/settings', createService(options));
  const service = app.service('settings');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
