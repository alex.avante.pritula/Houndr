const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const devices = sequelizeClient.define('devices', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    deviceId: {
      type      : Sequelize.STRING,
      field     : 'device_id',
      allowEmpty: false,
      require   : true
    },

    token: {
      type      : Sequelize.STRING,
      allowEmpty: false,
      require   : true
    },

    userId: {
      type      : Sequelize.INTEGER,
      field     : 'user_id',
      allowNull : false,
      require   : true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['token','device_id']
      }
    ],
    underscored: true,
    classMethods: {
      associate (models) {
        devices.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
      },

      getUserDevicesById(ids) {
        return this.findAll({
          where: {user_id: ids}
        });
      },
    }
  });

  return devices;
};
