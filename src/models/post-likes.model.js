const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const postLikes = sequelizeClient.define('post_likes', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    userId: {
      type      : Sequelize.INTEGER,
      field     : 'user_id',
      allowNull : false,
      require   : true
    },

    postId: {
      type      : Sequelize.INTEGER,
      field     : 'post_id',
      allowNull : false,
      require   : true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['post_id','user_id']
      }
    ],
    underscored: true,
    classMethods: {
      associate (models) {
        postLikes.belongsTo(models.posts, {
          as: 'post',
          foreignKey: 'post_id'
        });

        postLikes.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
      }
    }
  });

  return postLikes;
};
