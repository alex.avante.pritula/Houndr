const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const postAbuses = sequelizeClient.define('post_abuses', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    text: {
      type      : Sequelize.STRING,
      allowEmpty: true
    },

    userId: {
      type      : Sequelize.INTEGER,
      field     : 'user_id',
      allowNull : false,
      require   : true
    },

    postId: {
      type      : Sequelize.INTEGER,
      field     : 'post_id',
      allowNull : false,
      require   : true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    classMethods: {
      associate (models) {
        postAbuses.belongsTo(models.posts, {
          as: 'post',
          foreignKey: 'post_id'
        });
        postAbuses.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
      }
    }
  });

  return postAbuses;
};
