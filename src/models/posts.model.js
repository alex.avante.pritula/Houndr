const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const posts = sequelizeClient.define('posts', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    title: {
      type      : Sequelize.STRING,
      allowEmpty: false,
      require   : true
    },

    text: {
      type      : Sequelize.STRING,
      allowEmpty: true
    },

    post_code: {
      type      : Sequelize.STRING,
      allowEmpty: true
    },
    new_comments: {
      type      : Sequelize.BOOLEAN,
      allowEmpty: true
    },
    userId: {
      type      : Sequelize.INTEGER,
      field     : 'user_id',
      allowNull : false,
      require   : true
    },

    imageUrl: {
      type      : Sequelize.STRING,
      field     : 'image_url',
      allowEmpty: true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    // underscored: true,
    classMethods: {
      associate (models) {
        posts.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
        posts.hasMany(
          models.post_images, {
            as: 'images',
            foreignKey: 'post_id'
          }
       );
      },
      getPostOwner(postId) {
        return this.findOne({
          where: {id: postId},
        });
      },
      getPost(id) {
        return this.findOne({
          where: {id: id},
          include : [{
            model: sequelizeClient.models.users,
            'as': 'user',
            attributes: [
              'id',
              'image_url',
              ['name', 'username']
            ]
          },{
            model: sequelizeClient.models.post_images,
            'as': 'images'
          }],
          attributes: [
            'id',
            'image_url',
            'text',

            'title',
            'created_at',
            'updated_at'
          ]
        }).then(post => {
          if (post == null) {
            return {};
          }
          return post;
        });
      },

      getPostCount(user_id){
        return this.count({
          where:{
            user_id:user_id,
            new_comments:true
          }
        });
      },

      getPosts(_limit, _offset, userId, where) {
        return this.findAll({
          where: where,
          limit:  _limit,
          offset: _offset,
          order: '"created_at" DESC',
          include : [{
            model: sequelizeClient.models.users,
            'as': 'user',
            attributes: [
              'id',
              'image_url',
              'name'
            ]
          },{
            model: sequelizeClient.models.post_images,
            'as': 'images'
          }],
          replacements: {
            userId  : userId,
          },
          attributes: [
            'id',
            'user_id',
            'image_url',
            'text',
            'title',
            'created_at',
            'updated_at',
            [
              sequelizeClient.literal('(SELECT count("post_comments"."id") FROM "post_comments" WHERE "post_comments"."post_id" = "posts"."id")'), 'comment_count'
            ],
            [
              sequelizeClient.literal('(SELECT count("post_likes"."id") FROM "post_likes" WHERE "post_likes"."post_id" = "posts"."id")'), 'like_count'
            ],
            [
              sequelizeClient.literal('(SELECT CASE WHEN user_id = :userId THEN true ELSE false END AS is_liked FROM "post_likes" WHERE "post_likes"."post_id" = "posts"."id" AND "post_likes"."user_id" = :userId)'), 'is_liked'
            ]
          ],
        });
      },

      searchPosts(title, text, userId, _limit, _offset) {
        return this.findAll({
          where: {
            $and: [{
              title: {
                $like: '%' + title.toLowerCase() + '%'
              }
            }, {
              text: {
                $like: '%' + text.toLowerCase() + '%'
              }
            }]
          },
          include : [{
            model: sequelizeClient.models.users,
            as: 'user',
            attributes: [
              'id',
              'image_url',
              ['name', 'username']
            ]
          },{
            model: sequelizeClient.models.post_images,
            'as': 'images'
          }],
          replacements: {
            userId: userId,
            text: text,
            title: title,
          },
          attributes: [
            'id',
            'user_id',
            'image_url',
            'text',
            'title',
            'created_at',
            'updated_at',
            [
              sequelizeClient.literal('(SELECT count("post_comments"."id") FROM "post_comments" WHERE "post_comments"."post_id" = "posts"."id")'), 'comment_count'
            ],
            [
              sequelizeClient.literal('(SELECT count("post_likes"."id") FROM "post_likes" WHERE "post_likes"."post_id" = "posts"."id")'), 'like_count'
            ],
            [
              sequelizeClient.literal('(SELECT CASE WHEN user_id = :userId THEN true ELSE false END AS is_liked FROM "post_likes" WHERE "post_likes"."post_id" = "posts"."id" AND "post_likes"."user_id" = :userId)'), 'is_liked'
            ]
          ],
          limit:  _limit,
          offset: _offset,
          order: '"title" ASC',
        }).then(posts => {
          if (posts == null) {
            return [];
          }
          return posts;
        });
      }
    }
  });

  return posts;
};
