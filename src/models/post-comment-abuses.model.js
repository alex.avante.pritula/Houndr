const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const postCommentAbuses = sequelizeClient.define('post_comment_abuses', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    text: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    postCommentId: {
      type: Sequelize.INTEGER,
      field: 'post_comment_id',
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    classMethods: {
      associate (models) {
        postCommentAbuses.belongsTo(models.post_comments, {
          as: 'post_comment',
          foreignKey: 'post_comment_id'
        });
        postCommentAbuses.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
      }
    }
  });

  return postCommentAbuses;
};
