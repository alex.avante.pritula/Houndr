const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const settings = sequelizeClient.define('settings', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    key: {
      type: Sequelize.STRING,
      allowEmpty: false,
      require: true
    },

    value: {
      type: Sequelize.STRING,
      allowEmpty: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true,
    classMethods: {
      // eslint-disable-next-line no-unused-vars
      associate (models) {
      }
    }
  });

  return settings;
};
