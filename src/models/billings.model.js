const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const billings = sequelizeClient.define('billings', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    typeId: {
      type: Sequelize.INTEGER,
      field: 'type_id',
      allowNull: false,
      require: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    amount: {
      type: Sequelize.DECIMAL,
      allowEmpty: true,
      defaultValue: 0
    },

    description: {
      type: Sequelize.TEXT,
      allowEmpty: true
    },

    chargeId: {
      type: Sequelize.STRING,
      field: 'charge_id',
      allowEmpty: true,
      defaultValue: ''
    },

    transferId: {
      type: Sequelize.STRING,
      field: 'transfer_id',
      allowEmpty: true,
      defaultValue: ''
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        /*billings.belongsTo(models.billingTypes, {
         as: 'type',
         foreignKey: 'type_id'
         }); */

        /*this.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });*/

        this.hasMany(models.billings_sessions, {
            as: 'billings_sessions',
            foreignKey: 'billing_id'
          }
        );

        this.belongsToMany(models.sessions, {
          through: {
            model: models.billings_sessions
          },
          foreignKey: 'billing_id'
        });
      },
      createCharge(userId, chargeAmount, type, chargeId, options) {
        options = options || {};
        return sequelizeClient.models.billings.create({
          amount: chargeAmount,
          userId: userId,
          typeId: type,
          chargeId: chargeId
        },{
          transaction: options.transaction
        });
      },
      createTransfer(transferAmount, userId, type, transferId, options) {
        options = options || {};
        return sequelizeClient.models.billings.create({
          amount: transferAmount,
          userId: userId,
          typeId: type,
          transferId: transferId
        },{
          transaction: options.transaction
        });
      },
      createRefundCharge(refundAmount, userId, type, chargeId, options) {
        options = options || {};
        return sequelizeClient.models.billings.create({
          amount: refundAmount,
          userId: userId,
          typeId: type,
          chargeId: chargeId
        },{
          transaction: options.transaction
        });
      },

      getLastBillingBySessionId(sessionId, types) {
        return sequelizeClient.models.billings.findOne({
         include: [{
            model: sequelizeClient.models.billings_sessions,
            as: 'billings_sessions',
            "duplicating": false
          }],
          where: {
            '$billings_sessions.session_id$': sessionId,
            typeId: types
          },
          limit: 1,
          order: '"created_at" DESC'
        });
      },

      getBillingsBySessionId(id, limit, offset) {
        return sequelizeClient.models.billings.findAll({
          include: [{
            model: sequelizeClient.models.billings_sessions,
            as: 'billings_sessions',
            "duplicating": false
          }],
          where: {
            '$billings_sessions.session_id$': id
          },
          limit: limit,
          offset: offset
        });
      },

      getBillings(limit, offset) {
        return sequelizeClient.models.billings.findAll({
          limit: limit,
          offset: offset
        });
      }
    }
  });

  return billings;
};
