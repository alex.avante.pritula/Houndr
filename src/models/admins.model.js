const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const admins = sequelizeClient.define('admins', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    email: {
      type: Sequelize.STRING,
      allowEmpty: false,
      unique: true,
      require: true
    },

    password: {
      type: Sequelize.STRING,
      allowEmpty: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      // eslint-disable-next-line no-unused-vars
      associate (models) {
      }
    }
  });

  return admins;
};
