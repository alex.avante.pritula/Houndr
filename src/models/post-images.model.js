const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const postImages = sequelizeClient.define('post_images', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    postId: {
      type: Sequelize.INTEGER,
      field: 'post_id',
      allowNull: false,
      require: true
    },

    url: {
      type: Sequelize.STRING,
      allowEmpty: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        postImages.belongsTo(models.posts, {
          as: 'post',
          foreignKey: 'post_id'
        });
      }
    }
  });

  return postImages;
};
