const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const sessionDetails = sequelizeClient.define('session_details', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    sessionId: {
      type: Sequelize.INTEGER,
      field: 'session_id',
      allowNull: false,
      require: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    comment: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    rating: {
      type: Sequelize.INTEGER,
      allowEmpty: true
    },

    accountType: {
      type: Sequelize.ENUM,
      values: ['owner', 'provider'],
      field: 'account_type',
      allowEmpty: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        sessionDetails.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });

        sessionDetails.belongsTo(models.sessions, {
          as: 'session',
          foreignKey: 'session_id'
        });
      }
    }
  });

  return sessionDetails;
};
