const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const petsTempSessions = sequelizeClient.define('pets_temp_sessions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    petId: {
      type: Sequelize.INTEGER,
      field: 'pet_id',
      allowNull: false,
      require: true
    },

    tempSessionId: {
      type: Sequelize.INTEGER,
      field: 'temp_session_id',
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['temp_session_id', 'pet_id']
      }
    ],
    underscored: true,
    classMethods: {
      associate (models) {
        petsTempSessions.belongsTo(models.pets, {
          as: 'pet',
          foreignKey: 'pet_id'
        });

        this.belongsTo(models.temp_sessions, {
          as: 'temp_sessions',
          foreignKey: 'temp_session_id'
        });
      }
    }
  });

  return petsTempSessions;
};
