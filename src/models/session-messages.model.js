// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const sessionMessages = sequelizeClient.define('session_messages', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    senderId: {
      type: Sequelize.INTEGER,
      field: 'sender_id',
      allowNull: false,
      require: true
    },

    receiverId: {
      type: Sequelize.INTEGER,
      field: 'receiver_id',
      allowNull: false,
      require: true
    },

    sessionId: {
      type: Sequelize.INTEGER,
      field: 'session_id',
      allowNull: false,
      require: true
    },

    message: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    classMethods: {
      associate (models) {
        sessionMessages.belongsTo(models.sessions, {
          as: 'session',
          foreignKey: 'session_id'
        });
        /*sessionMessages.belongsTo(models.users, {
          as: 'sender',
          foreignKey: 'user_id'
        });
        sessionMessages.belongsTo(models.users, {
          as: 'receiver',
          foreignKey: 'user_id'
        });*/
      }
    }
  });

  return sessionMessages;
};
