const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const favorites = sequelizeClient.define('favorites', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    favoriteUserId: {
      type: Sequelize.INTEGER,
      field: 'favorite_user_id',
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        favorites.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });

        favorites.belongsTo(models.users, {
          as: 'favoriteUser',
          foreignKey: 'favorite_user_id'
        });
      }
    }
  });

  return favorites;
};
