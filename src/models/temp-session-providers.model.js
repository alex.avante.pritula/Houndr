const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const tempSessionProviders = sequelizeClient.define('temp_session_providers', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    providerId: {
      type: Sequelize.INTEGER,
      field: 'provider_id',
      allowNull: false,
      require: true
    },

    tempSessionId: {
      type: Sequelize.INTEGER,
      field: 'temp_session_id',
      allowNull: false,
      require: true
    },

    status: {
      type: Sequelize.ENUM,
      values: ['await', 'agreed', 'rejected'],
      allowEmpty: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['temp_session_id', 'provider_id']
      }
    ],
    underscored: true,
    classMethods: {
      associate (models) {
        this.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'provider_id'
        });
        this.belongsTo(models.temp_sessions, {
          as: 'temp_sessions',
          foreignKey: 'temp_session_id'
        });
      },
      getCountSessionsByRangeDate(startDateTime, endDateTime, providerId, tempSessionId, sessionId) {
        let replacements = {
          startDateTime: startDateTime,
          endDateTime: endDateTime,
          providerId: providerId,
        };

        if (typeof(sessionId) !== "undefined" && sessionId !== null) {
          replacements.sessionId = sessionId;
        }

        if (typeof(tempSessionId) !== 'undefined' && tempSessionId !== null) {
          replacements.tempSessionId = tempSessionId;
        }

        let options = {
          type: sequelizeClient.QueryTypes.SELECT,
          replacements: replacements,
          logging: function (str) {
            console.log(str);
          }
        };

        let query = `
            SELECT count(*) as count
            FROM temp_session_providers tsp
            LEFT JOIN temp_sessions ts ON (ts.id = tsp.temp_session_id)
            LEFT JOIN sessions s ON (s.provider_id = tsp.provider_id)
            WHERE tsp.provider_id = :providerId 
            AND ((:startDateTime <= ts.end_date_time) AND (:endDateTime >= ts.start_date_time)
                OR (:startDateTime <= s.end_date_time) AND (:endDateTime >= s.start_date_time))
            `;

        if (typeof(sessionId) !== "undefined" && sessionId !== null) {
          query = query + ` AND s.id <> :sessionId `;
        }

        if (typeof(tempSessionId) !== 'undefined' && tempSessionId !== null) {
          query = query + ` AND tsp.temp_session_id <> :tempSessionId `;
        }

        return sequelizeClient.query(query, options);
      },
      getProviderInTempSession(tempSessionId, providerId) {
        return sequelizeClient.models.temp_session_providers.findOne({
          where: {
            temp_session_id: tempSessionId,
            provider_id: providerId
          },
          include: [{
            model: sequelizeClient.models.temp_sessions,
            'as': 'temp_sessions',
            foreignKey: 'temp_session_id'
          }],
        });
      },
      getTempSessionByIdAndStatus(id, status, limit, offset) {
        return sequelizeClient.models.temp_session_providers.findAll({
          where: {
            tempSessionId: id,
            status: status
          },
          limit: limit,
          offset: offset,
          include: [{
            model: sequelizeClient.models.users,
            as: 'user',
            attributes: [
              'imageUrl',
              'latitude',
              'longitude',
              'name'
            ]
          }]
        });
      }
    }
  });

  return tempSessionProviders;
};
