const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const sessionActivities = sequelizeClient.define('session_activities', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    sessionId: {
      type: Sequelize.INTEGER,
      field: 'session_id',
      allowNull: false,
      require: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    typeId: {
      type: Sequelize.INTEGER,
      field: 'type_id',
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        sessionActivities.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });

        sessionActivities.belongsTo(models.sessions, {
          as: 'session',
          foreignKey: 'session_id'
        });

        sessionActivities.belongsTo(models.activity_types, {
          as: 'type',
          foreignKey: 'type_id'
        });
      }
    }
  });

  return sessionActivities;
};
