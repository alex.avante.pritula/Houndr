const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const billingsSessions = sequelizeClient.define('billings_sessions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    sessionId: {
      type: Sequelize.INTEGER,
      field: 'session_id',
      allowNull: false,
      require: true
    },

    billingId: {
      type: Sequelize.INTEGER,
      field: 'billing_id',
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['session_id', 'billing_id']
      }
    ],
    underscored: true,
    classMethods: {
      associate (models) {
        this.belongsTo(models.billings, {
          as: 'billings',
          foreignKey: 'billing_id'
        });

       /* this.belongsTo(models.sessions, {
          as: 'sessions',
          foreignKey: 'session_id'
        });*/
      },

      connectBillingToSession(billingId, sessionId, options) {
        options = options || {};
        return sequelizeClient.models.billings_sessions.create({
          billingId: billingId,
          sessionId: sessionId
        },{
          transaction: options.transaction
        });
      },
    }
  });

  return billingsSessions;
};

