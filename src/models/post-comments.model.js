const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const postComments = sequelizeClient.define('post_comments', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    postId: {
      type     : Sequelize.INTEGER,
      field    : 'post_id',
      allowNull: false,
      require  : true
    },

    userId: {
      type      : Sequelize.INTEGER,
      field     : 'user_id',
      allowNull : false,
      require   : true
    },

    text: {
      type      : Sequelize.STRING,
      allowEmpty: true
    },
    img_url: {
      type      : Sequelize.STRING,
      allowEmpty: true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        postComments.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });

        postComments.belongsTo(models.posts, {
          as: 'post',
          foreignKey: 'post_id'
        });
      }
    }
  });

  return postComments;
};
