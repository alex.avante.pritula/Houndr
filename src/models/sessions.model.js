const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const constants = app.get('constants');
  const sessions = sequelizeClient.define('sessions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    tax: {
      type: Sequelize.FLOAT,
      allowEmpty: false,
      require: true
    },

    startPoint: {
      type: Sequelize.STRING,
      field: 'start_point',
      allowEmpty: true
    },

    isCancelled: {
      type: Sequelize.BOOLEAN,
      field: 'is_cancelled',
      allowEmpty: false,
      defaultValue: false
    },

    longitude: {
      type: Sequelize.FLOAT,
      allowEmpty: true
    },

    latitude: {
      type: Sequelize.FLOAT,
      allowEmpty: true
    },

    ownerId: {
      type: Sequelize.INTEGER,
      field: 'owner_id',
      allowNull: false,
      require: true
    },

    providerId: {
      type: Sequelize.INTEGER,
      field: 'provider_id',
      allowNull: false,
      require: true
    },

    endDateTime: {
      type: Sequelize.DATE,
      field: 'end_date_time',
      allowNull: false,
      require: true
    },

    startDateTime: {
      type: Sequelize.DATE,
      field: 'start_date_time',
      allowNull: false,
      require: true
    },

    isClosed: {
      type: Sequelize.BOOLEAN,
      field: 'is_closed',
      allowEmpty: false,
      defaultValue: false
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        this.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'owner_id'
        });

        this.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'provider_id'
        });

        sessions.belongsToMany(models.pets, {
          through: {
            model: models.pets_sessions
          },
          foreignKey: 'session_id',
        });

        this.belongsToMany(models.billings, {
          through: {
            model: models.billings_sessions
          },
          foreignKey: 'session_id'
        });

        this.hasMany(models.devices, {
          as: 'devices',
          foreignKey: 'user_id',
          sourceKey: 'provider_id'
        });
      },
      getSessionById(id) {
        return sequelizeClient.models.sessions.find({
          where: {
            id: id
          },
          include: [{
            model: sequelizeClient.models.pets,
            as: 'pets',
            through: 'pet_id'
          }],
        });
      },

      closeSession(ids) {
        return sequelizeClient.models.sessions.update({
          isClosed: true
        }, {
          where: {
            id: ids
          }
        });
      },
      getSessionsForPayment() {
        let countDaysAfterSession = 1;
        return sequelizeClient.models.sessions.findAll({
          where: {
            is_cancelled: false,
            is_closed: false,
            end_date_time: {
              $lte: sequelizeClient.literal("NOW() - INTERVAL '1' DAY")
            }
          },
          include: [{
            model: sequelizeClient.models.billings,
            as: 'billings',
            where: {
              typeId: [
                constants.BILLING_TYPES.PROLONG_SESSION_OWNER_PAYMENT,
                constants.BILLING_TYPES.SESSION_OWNER_PAYMENT
              ]
            },
          }, {
            model: sequelizeClient.models.users,
            as: 'user',
          }, {
            model: sequelizeClient.models.devices,
            as: 'devices',
          }],
        });
      }
    }
  });

  return sessions;
};
