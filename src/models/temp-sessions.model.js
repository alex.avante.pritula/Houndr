const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const tempSessions = sequelizeClient.define('temp_sessions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    ownerId: {
      type: Sequelize.INTEGER,
      field: 'owner_id',
      allowNull: false,
      require: true
    },

    endDateTime: {
      type: Sequelize.DATE,
      field: 'end_date_time',
      allowNull: false,
      require: true
    },

    startDateTime: {
      type: Sequelize.DATE,
      field: 'start_date_time',
      allowNull: false,
      require: true
    },

    longitude: {
      type: Sequelize.DECIMAL,
      allowEmpty: true
    },

    latitude: {
      type: Sequelize.DECIMAL,
      allowEmpty: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        this.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'owner_id'
        });

        this.hasMany(models.pets_temp_sessions, {
            as: 'pets_temp_sessions'
          }
        );

        this.hasMany(models.temp_session_providers, {
            as: 'temp_session_providers',
            foreignKey: 'id'
          }
        );
      },
      getTempSessionById(id) {
        return sequelizeClient.models.temp_sessions.findOne({
          where: {
            id: id
          },
          include: [{
            model: sequelizeClient.models.pets_temp_sessions,
            as: 'pets_temp_sessions'
          }],
        });
      }
    }
  });

  return tempSessions;
};
