const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const activityTypes = sequelizeClient.define('activity_types', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    description: {
      type: Sequelize.STRING,
      allowEmpty: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      // eslint-disable-next-line no-unused-vars
      associate (models) {

      }
    }
  });

  return activityTypes;
};
