const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const deliveryNotifications = sequelizeClient.define('delivery_notifications', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },

    pushType: {
      type: Sequelize.INTEGER,
      field: 'push_type',
      allowEmpty: false
    },

    data: {
      type: Sequelize.TEXT,
      allowEmpty: true
    },

    deviceId: {
      type: Sequelize.STRING,
      field: 'device_id',
      allowEmpty: false
    },

    isSent: {
      type: Sequelize.BOOLEAN,
      field: 'is_sent',
      allowNull: false,
      defaultValue: false
    },

    deliveryDateTime: {
      type: Sequelize.DATE,
      field: 'delivery_date_time'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    // underscored: true,
    classMethods: {
      associate (models) {
        deliveryNotifications.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });
      },
      setIsSentNotification(ids) {
        return sequelizeClient.models.delivery_notifications.update({
          is_sent: 1
        }, {
          where: {
            id: {
              in: ids
            }
          }
        });
      },
      createNotification(settings) {
        if (!settings) {
          error = new Error('invalid arguments');
          error.status = 400;
          return Promise.reject(error);
        }

        if (!settings.userData) {
          error = new Error('userData is required');
          error.status = 400;
          return Promise.reject(error);
        }

        let objectData = '';
        if (settings.data) objectData = settings.data.message;

        if (!settings.pushType) {
          error = new Error('pushType is invalid');
          error.status = 400;
          return Promise.reject(error);
        }

        let data = {
          push_type: settings.pushType,
          data: JSON.stringify(objectData),
          delivery_date_time: settings.deliveryDateTime
        };

        let queryData = [];
        for (let i = 0; i < settings.userData.length; i++) {
          let tempData = Object.assign({}, data);
          tempData["user_id"] = settings.userData[i]["user_id"];
          tempData["device_id"] = settings.userData[i]["device_id"];
          queryData.push(tempData);
        }

        return this.bulkCreate(queryData, {})
          .then(function (deliveryNotification) {
            return deliveryNotification;
          });
      }
    }
  });

  return deliveryNotifications;
};
