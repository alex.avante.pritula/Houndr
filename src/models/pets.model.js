const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const pets = sequelizeClient.define('pets', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    breed: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    sex: {
      type: Sequelize.ENUM,
      values: ['male', 'female'],
      allowEmpty: true
    },

    isVactinated: {
      type: Sequelize.STRING,
      field: 'is_vactinated',
      defaultValue: false,
    },

    imageUrl: {
      type: Sequelize.STRING,
      field: 'image_url',
      allowEmpty: true
    },

    userId: {
      type: Sequelize.INTEGER,
      field: 'user_id',
      allowNull: false,
      require: true
    },
    age:{
      type: Sequelize.INTEGER
    },
    dogs_characteristic:{
      type: Sequelize.STRING
    },
    dogs_instruction:{
      type: Sequelize.STRING
    },
    fun_Energy:{
      type: Sequelize.INTEGER
    },
    fun_Training:{
      type: Sequelize.INTEGER
    },
    fun_Offleash:{
      type: Sequelize.INTEGER
    },
    fun_Kids:{
      type: Sequelize.INTEGER
    },
    fun_Cats:{
      type: Sequelize.INTEGER
    },
    vet_name:{
      type: Sequelize.STRING
    },
    vet_location:{
      type: Sequelize.STRING
    },
    vet_phone:{
      type: Sequelize.STRING
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        pets.belongsTo(models.users, {
          as: 'user',
          foreignKey: 'user_id'
        });

        pets.belongsToMany(models.sessions, {
          through: {
            model: models.pets_sessions
          },
          foreignKey: 'pet_id',
        });
      },
      getUserPets(userId, _limit, _offset) {
        return this.findAll({
          limit:  _limit,
          offset: _offset,
          replacements: {
            userId  : userId,
          },
          attributes: [
            'id',
            'user_id',
            'is_vactinated',
            'sex',
            'breed',
            'name',
            'age',
            'dogs_characteristic',
            'dogs_instruction',
            'fun_Energy',
            'fun_Training',
            'fun_Offleash',
            'fun_Kids',
            'fun_Cats',
            'vet_name',
            'vet_location',
            'vet_phone',
            'created_at',
            'updated_at',
          ],
        });
      },
    }
  });

  return pets;
};
