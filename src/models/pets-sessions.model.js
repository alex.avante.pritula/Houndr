const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const petsSessions = sequelizeClient.define('pets_sessions', {
    id: {
      type         : Sequelize.INTEGER,
      primaryKey   : true,
      autoIncrement: true
    },

    petId: {
      type     : Sequelize.INTEGER,
      field    : 'pet_id',
      allowNull: false,
      require  : true
    },

    sessionId: {
      type     : Sequelize.INTEGER,
      field    : 'session_id',
      allowNull: false,
      require  : true
    },

    createdAt: {
      type : Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type : Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true,
    classMethods: {
      associate (models) {
      }
    }
  });

  return petsSessions;
};
