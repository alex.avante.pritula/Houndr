const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');

  const users = sequelizeClient.define('users', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    email: {
      type: Sequelize.STRING,
      allowNull: true,
      unique: true
    },

    password: {
      type: Sequelize.STRING,
      allowNull: true
    },

    googleId: {
      type: Sequelize.STRING,
      field: 'google_id',
      allowNull: true
    },

    facebookId: {
      type: Sequelize.STRING,
      field: 'facebook_id',
      allowNull: true
    },

    twitterId: {
      type: Sequelize.STRING,
      field: 'twitter_id',
      allowNull: true
    },

    name: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    isVerified: {
      type: Sequelize.BOOLEAN,
      field: 'is_verified',
      allowNull: false,
      defaultValue: false
    },

    age: {
      type: Sequelize.INTEGER,
      allowEmpty: true
    },

    address: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    longitude: {
      type: Sequelize.FLOAT,
      allowEmpty: true
    },

    latitude: {
      type: Sequelize.FLOAT,
      allowEmpty: true
    },

    isProvider: {
      type: Sequelize.BOOLEAN,
      field: 'is_provider',
      allowNull: false,
      defaultValue: false
    },

    hasNotifications: {
      type: Sequelize.BOOLEAN,
      field: 'has_notifications',
      allowNull: false,
      defaultValue: true
    },

    imageUrl: {
      type: Sequelize.STRING,
      field: 'image_url',
      allowEmpty: true
    },
    last_name: {
      type: Sequelize.STRING,
      field: 'last_name',
      allowEmpty: true
    },
    restore_token: {
      type: Sequelize.STRING,
      field: 'restore_token',
      allowEmpty: true
    },
    post_code: {
      type: Sequelize.STRING,
      field: 'post_code',
      allowEmpty: true
    },
    available_as_owner: {
      type: Sequelize.STRING,
      field: 'post_code',
      allowEmpty: true
    },
    available_as_provider: {
      type: Sequelize.STRING,
      field: 'post_code',
      allowEmpty: true
    },
    is_have_dog: {
      type: Sequelize.BOOLEAN,
      field: 'is_have_dog',
      allowEmpty: true
    },

    about_myself: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    phone: {
      type: Sequelize.STRING,
      allowEmpty: true
    },

    lastActivity: {
      type: Sequelize.DATE,
      field: 'last_activity',
      allowEmpty: true
    },

    countReviews: {
      type: Sequelize.INTEGER,
      field: 'count_reviews'
    },

    stripeCustomerId: {
      type: Sequelize.STRING,
      field: 'stripe_customer_id',
      allowEmpty: true
    },

    stripeAccountId: {
      type: Sequelize.STRING,
      field: 'stripe_account_id',
      allowEmpty: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true,
    classMethods: {
      associate (models) {
        users.hasMany(models.sessions, {
          as: 'session',
          foreignKey: 'owner_id'
        });

        users.hasMany(models.sessions, {
          as: 'session',
          foreignKey: 'provider_id'
        });
      },

      getUserByEmail(email) {
        return sequelizeClient.models.users.findOne({
          where: { email: email },
          attributes: ['id', 'email'],
        });
      },

      findByToken(token) {
        return sequelizeClient.models.users.findOne({
          where: { restore_token: token },
          attributes: ['id', 'email'],
        });
      },

      updateRestoreTocen(id, token){
        return sequelizeClient.models.users.update({
          restore_token:token
        },{
          where:{id:id}
        });
      },

      getNeighbors(params){
        let options = {
          type: sequelizeClient.QueryTypes.SELECT,
          replacements: params,
          logging: function (str) {
           //console.log(str);
          }
        };

        return sequelizeClient.query(`
            SELECT * FROM (
              SELECT DISTINCT
                "user".id,
                (
                  acos(sin(:latitude) * sin("user".latitude) +
                       cos(:latitude) * cos("user".latitude) *
                       cos(:longitude - "user".longitude))
                ) "distance",

                (
                  SELECT "session_with_owner".end_date_time
                  FROM sessions "session_with_owner"
                  WHERE "session_with_owner".provider_id = "user".id
                    AND "session_with_owner".owner_id = :ownerId
                    AND "session_with_owner".end_date_time <= NOW()
                ) "lastCommonSession",

                (
                  SELECT COUNT(*)
                  FROM sessions "session_cancelled"
                  WHERE "session_cancelled".provider_id = "user".id
                    AND "session_cancelled".is_cancelled = TRUE
                ) "cancelledSessions"

              FROM users "user"
                INNER JOIN sessions "crossed_session"
                  ON (
                  ("crossed_session".provider_id <> "user".id
                  OR
                  ("crossed_session".provider_id = "user".id
                   AND "crossed_session".start_date_time > :end
                   AND "crossed_session".end_date_time < :start)))

              LEFT JOIN  (
                 SELECT * FROM temp_session_providers 
                 INNER JOIN temp_sessions  "temp_crossed_session"
                 ON "temp_crossed_session".start_date_time > :end
                 AND "temp_crossed_session".end_date_time < :start 
                 AND "temp_crossed_session".id = temp_session_providers.temp_session_id
              ) "temp_provider"
               ON "temp_provider".provider_id = "user".id
               AND "temp_provider".provider_id <> "crossed_session".provider_id
                
              WHERE "user".id != :ownerId AND "user".is_provider = true 
                AND ("user".avalable_from <= (:start)::time OR "user".avalable_from is NULL)
                AND ("user".avalable_to >= (:end)::time OR "user".avalable_to is NULL)
          ) "neighbors"
          WHERE "neighbors"."distance" <= :radius
          ORDER BY
            "neighbors"."lastCommonSession" DESC,
            "neighbors"."cancelledSessions" DESC,
            "neighbors"."distance" ASC
        `, options);
      },

      updateStripeCustomerId(customerId, userId) {
        return sequelizeClient.models.users.update({
          stripeCustomerId: customerId
        }, {
          where: {
            id: userId
          }
        });
      },

      updateStripeAccountId(accountId, userId) {
        return sequelizeClient.models.users.update({
          stripeAccountId: accountId
        }, {
          where: {
            id: userId
          }
        });
      },

      getUserById(id) {
        return sequelizeClient.models.users.findOne({
          where: {
            id: id
          }
        });
      },
    }
  });

  return users;
};
