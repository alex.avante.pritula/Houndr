const _ = require('lodash');
const path = require('path');
const createBillingsModel = require('../models/billings.model');
const createUsersModel = require('../models/users.model');
const createBillingsSessionsModel = require('../models/billings-sessions.model');
const stripe = require("stripe");
const util = require('util');

class stripeService {
  constructor(app) {
    this.app = app;

    this.stripe = stripe(
      this.app.get('stripe').secret_key
    );

    this.billingsModel = createBillingsModel(app);
    this.usersModel = createUsersModel(app);
    this.billingsSessionsModel = createBillingsSessionsModel(app);
  }

  createCustomer(cardNumber, expMonth, expYear, cvv) {
    let customerCard = {
      object: "card",
      number: cardNumber,
      exp_month: expMonth,
      exp_year: expYear,
      cvc: cvv
    };

    let self = this;
    return new Promise(function(resolve, reject) {
      self.stripe.customers.create({
        description: 'create customer',
        source: customerCard // obtained with Stripe.js
      }, function (err, customer) {
        if (err) {
          return reject(err);
        }

        return resolve(customer);
      });
    });
  }

  refundCharge(amount, chargeId) {
    let refundAmount = amount * 100;
    return this.stripe.refunds.create({
      charge: chargeId,
      amount: refundAmount
    });
  }


  createCharge(chargeAmount, customerId) {
    chargeAmount = chargeAmount * 100;
      let chargeData = {
        amount: chargeAmount,
        currency: "usd",
        customer: customerId,
        description: "charge"
      };

      return this.stripe.charges.create(chargeData);
  }

  createTransfer(transferAmount, stripeAccountId) {
    transferAmount = transferAmount * 100;
    return this.stripe.transfers.create({
        amount: transferAmount,
        currency: "usd",
        destination: stripeAccountId
    });
  }

  createStripeVirtualAccount(params) {
    return this.stripe.accounts.create({
      type: "standard",
      country: params.country,
      email: params.email
    });
  }

  removeStripeVirtualAccount(accountId) {
    return this.stripe.accounts.del(accountId);
  }
}

module.exports = function () {
  const app = this;
  app.set('stripeService', new stripeService(app));
};
