const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      text: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      post_id: {
        description: '',
        type: 'integer',
        required: true
      }
    }
  });

  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

module.exports = {
  create   : create
};
