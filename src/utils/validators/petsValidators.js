const revalidator = require('revalidator');


function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        type: 'string',
        required: true,
      },

      userId: {
        type: 'integer',
        required: true,
      },
      age: {
        type: 'integer',
        required: false,
      },
      dogs_characteristic: {
        type: 'string',
        maxLength: 500,
        required: false,
      },
      dogs_instruction: {
        type: 'string',
        maxLength: 500,
        required: false,
      },
      fun_Energy: {
        type: 'integer',
        required: true,
        maximum: 10
      },
      fun_Training: {
        type: 'integer',
        required: true,
        maximum: 10
      },
      fun_Offleash: {
        type: 'integer',
        required: true,
        maximum: 10
      },
      fun_Kids: {
        type: 'integer',
        required: true,
        maximum: 10
      },
      fun_Cats: {
        type: 'integer',
        required: true,
        maximum: 10
      },

    }
  });

  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

module.exports = {
  create: create
};
