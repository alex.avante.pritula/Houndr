module.exports = {
  user: require('./userValidators'),
  post: require('./postValidators'),
  pet: require('./petsValidators'),
  postComment: require('./postCommentValidators'),
  session: require('./sessionValidators'),
  sessionDetails: require('./sessionDetailsValidators')
};
