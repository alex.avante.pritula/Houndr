const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true,
      },
      password: {
        description: '',
        type: 'string',
        minLength: 6,
        required: true,
      },
      email: {
        description: '',
        format: 'email',
        type: 'string',
        required: true,
      },
      available_as_owner: {
        enum: ['Morning',
          'Afternoon',
          'Evening',
          'Weekends',
          'Short trips',
          'Longer trips ']
      },
      available_as_provider: {
        enum: ['Morning',
          'Afternoon',
          'Evening',
          'Weekends',
          'Short trips',
          'Longer trips ']
      },
    }
  });

  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

function update(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
      },
      age: {
        description: '',
        type: 'integer',
      },
      address: {
        description: '',
        type: 'string',
        maxLength: 255
      },
      longitude: {
        description: '',
        type: 'number',
      },
      latitude: {
        description: '',
        type: 'number',
      },
      is_provider: {
        description: '',
        type: 'boolean',
      },
      has_notifications: {
        description: '',
        type: 'boolean',
      },
      available_as_owner: {
        enum: ['Morning',
          'Afternoon',
          'Evening',
          'Weekends',
          'Short trips',
          'Longer trips']
      },
      available_as_provider: {
        enum: ['Morning',
          'Afternoon',
          'Evening',
          'Weekends',
          'Short trips',
          'Longer trips']
      },
    }
  });

  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

function updateAccount(data) {
  let validator = revalidator.validate(data, {
    properties: {
      country: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      currency: {
        type: 'string',
        allowEmpty: false,
        required: true
      }
    }
  });

  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

function updateCustomer(data) {
  let validator = revalidator.validate(data, {
    properties: {
      card_number: {
        type: 'string',
        pattern: /^\d+$/,
        allowEmpty: false,
        required: true,
        messages: {
          pattern: 'is not a valid'
        }
      },
      exp_month: {
        type: 'string',
        pattern: /^(0?[1-9]|1[012])$/,
        allowEmpty: false,
        required: true,
        messages: {
          pattern: 'is not a valid'
        }
      },
      exp_year: {
        type: 'string',
        pattern: /^\d{2}(\d{2})?$/,
        allowEmpty: false,
        required: true,
        messages: {
          pattern: 'is not a valid'
        }
      },
      cvv: {
        type: 'string',
        pattern: /^\d{3}$/,
        allowEmpty: false,
        required: true,
        messages: {
          pattern: 'is not a valid'
        }
      }
    }
  });
  //todo - need to modify validator(there is common logic for all validators)
  let message = '';
  if (!validator.valid) {
    message = [
      validator.errors[0].property,
      validator.errors[0].message,
    ].join(' ');
  }

  return message;
}

module.exports = {
  create: create,
  update: update,
  updateAccount: updateAccount,
  updateCustomer: updateCustomer
};
