const AWS = require('aws-sdk');
const Promise = require('bluebird');
const path = require('path');
const uuid = require('uuid');
const mime = require('mime');

class s3Service {
  constructor(configs){
    this.configs = Object.assign({}, configs);
    AWS.config.update(configs);
    this.s3 = new AWS.S3();
  }

  uploadImage(target, data){
    const { bucket } =  this.configs;
    const { mimetype, buffer } = data;
    const ext = mime.extension(mimetype);

    let params = {
      Bucket: bucket,
      Key: path.join(target, uuid.v1() + '.' + ext),
      ContentType: mimetype,
      Body: new Buffer(buffer),
      ACL: 'public-read'
    };

    return this.s3.upload(params)
      .promise().then((result)=>{
        return {
          url : result.Location,
          name: result.Key
        };
      }); 
  }

  uploadImages(target, files){
    return Promise.mapSeries(files, (file)=>{
      return this.uploadImage(target, file);
    });
  }

  deleteImage(target, name){
    const {  bucket } =  this.configs;

    let options = {
      Bucket: bucket,
      Key   : path.join(target, name)
    };

    return this.s3.deleteObject(options).promise()
    .then((result)=>{
      return { isDeleted: result.DeleteMarker };
    });
  }
}

module.exports = function () {
  const app = this;
  const aws = app.get('aws');
  app.set('s3Service', new s3Service(aws));
};