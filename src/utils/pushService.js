const _ = require('lodash');
const Pusher = require('./pusher');
const path = require('path');
const async = require('async');
const createDeliveryNotificationsModel = require('../models/delivery-notifications.model');

class deliveryNotificationsService {
  constructor(app) {
    const push = app.get('push');
    this.app = app;
    this.deliveryNotificationsModel = createDeliveryNotificationsModel(app);
    this.pusher = new Pusher(push);
  }

  send(push, recipients) {
    const message = push.message;
    const options = push.options;

    if(!message) return;

    this.sendPushByTokens(message, recipients);
  }

  /**
   * Iterate device token and send pushes without query.
   * @param message
   * @param {Array} recipients
   */
  sendPushByTokens(message, recipients) {
    let _sendPush = this.pusher.sendPushes();
    let iterateCount = recipients.length;
    async.times(iterateCount, function (iteration, next) {
      _sendPush(message, recipients, next);
    }, this.complete.bind(this, message));
  }

  /**
   * Complete sending pushes handler.
   * @param message
   * @param err
   * @param results
   */
  complete(message, err, results) {
    if (err) {
      return console.log(new Date(), 'error : PushNotificationError', err);
    }
    console.log(new Date(), 'info : PushNotifications');

    if (typeof(message["ids"]) !== "undefined") {
      //change to model(deliveryNotifications)
      this.deliveryNotificationsModel.setIsSentNotification(message["ids"]);
    }

    results.forEach(function (result) {
      console.log('info : PushNotification ', result);
    });
  }
}

module.exports = function () {
  const app = this;
  app.set('deliveryNotificationsService', new deliveryNotificationsService(app));
};
