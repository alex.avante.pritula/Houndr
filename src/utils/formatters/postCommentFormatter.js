const _ = require('lodash');

function getPostComments(postComments) {
  let obj = [];
  for (const postComment of postComments) {

    if (!_.some(obj, function (currentPostComment) {
      return postComment.id === currentPostComment.id
    })) {
      obj.push({
        id: postComment.id,
        text: postComment.text,
        post_id: postComment.post_id,
        user_id: postComment['user.id'],
        username: postComment['user.name'],
        user_avatar: postComment['user.imageUrl'],
        created_at: postComment.created_at,
        updated_at: postComment.updated_at,
      });
    }
  }
  return obj;
}

function getPostComment(postComments) {
  let obj = [];
  for (const postComment of postComments) {

    if (!_.some(obj, function (currentPostComment) {
      return postComment.id === currentPostComment.id
    })) {
      obj.push({
        id: postComment.id,
        text: postComment.text,
        post_id: postComment.post_id,
        user_id: postComment['user.id'],
        username: postComment['user.name'],
        user_avatar: postComment['user.imageUrl'],
        email: postComment['user.email'],
        created_at: postComment.created_at,
        updated_at: postComment.updated_at,
      });
    }
  }
  return _.first(obj);
}

module.exports = {
  getPostComments: getPostComments,
  getPostComment: getPostComment
};
