function getUsers(data) {
  return data.map(function (dataItem) {
    return {
      id: dataItem.id,
      facebook_id: dataItem.facebook_id,
      twitter_id: dataItem.twitter_id,
      google_id: dataItem.google_id,
      email: dataItem.email,
      age: dataItem.age,
      name: dataItem.name,
      address: dataItem.address,
      longitude: dataItem.longitude,
      latitude: dataItem.latitude,
      has_notifications: dataItem.has_notifications,
      image_url: dataItem.image_url,
      last_activity: dataItem.last_activity,
      post_code: dataItem.postCode,
      available_as_owner: dataItem.available_as_owner,
      available_as_provider: dataItem.available_as_provider,
      about_myself: dataItem.about_myself,
      phone: dataItem.phone,
      created_at: dataItem.created_at,
      updated_at: dataItem.updated_at
    };
  });
}

function getUser(dataItem) {
  return {
    id: dataItem.id,
    facebook_id: dataItem.facebookId,
    twitter_id: dataItem.twitterId,
    google_id: dataItem.googleId,
    email: dataItem.email,
    age: dataItem.age,
    name: dataItem.name,
    address: dataItem.address,
    longitude: dataItem.longitude,
    latitude: dataItem.latitude,
    has_notifications: dataItem.hasNotifications,
    image_url: dataItem.imageUrl,
    last_activity: dataItem.lastActivity,
    post_code: dataItem.postCode,
    available_as_owner: dataItem.available_as_owner,
    available_as_provider: dataItem.available_as_provider,
    about_myself: dataItem.about_myself,
    phone: dataItem.phone,
    created_at: dataItem.createdAt,
    updated_at: dataItem.updatedAt
  };
}

function createUser(dataItem) {
  return {
    access_token: dataItem.accessToken
  };
}

function getFavoriteUsers(data){
  return data.map(function (dataItem) {
    const { favoriteUser } = dataItem;
    let item = favoriteUser.toJSON();
    item.favoriteUserId = dataItem.id;
    if(!item.count_reviews) item.count_reviews = 0;
    return item;
  });
}

module.exports = {
  getUsers: getUsers,
  getUser: getUser,
  getFavoriteUsers: getFavoriteUsers,
  createUser: createUser
};
