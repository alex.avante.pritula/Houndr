module.exports = {
  user: require('./userFormatter'),
  post: require('./postFormatter'),
  session: require('./sessionFormatter'),
  postComment: require('./postCommentFormatter'),
};
