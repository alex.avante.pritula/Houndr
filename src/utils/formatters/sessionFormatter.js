function getNeighbors(neighbors) {
  return neighbors.map((neighbor)=>{
    return {
      id: neighbor.providerId,
      image_url: neighbor.user.imageUrl,
      latitude: neighbor.user.latitude,
      longitude: neighbor.user.longitude,
      temp_provider_id: neighbor.id,
      status: neighbor.status
    };
  });
}

module.exports = {
  getNeighbors: getNeighbors
};
