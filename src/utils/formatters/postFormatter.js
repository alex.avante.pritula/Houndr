const _ = require('lodash');

function getPosts(posts) {
  let obj = [];
  for (const post of posts) {
    if (!_.some(obj, function (currentPost) { return post.id === currentPost.id })) {
      obj.push({
        id: post.id,
        title: post.title,
        body: post.body,
        user_id: post['user.id'],
        username: post['user.name'],
        user_avatar: post['user.imageUrl'],
        email: post['user.email'],
        images: [],
        created_at: post.created_at,
        updated_at: post.updated_at,
      });
    }

    obj = _.map(obj, function (currentPost) {
      if (currentPost.id === post.id && post['images.id'] !== null) {
        currentPost.images.push({
          id: post['images.id'],
          url: post['images.url']
        });
      }
      return currentPost;
    });

  }
  return obj;
}


function getPost(posts) {
  let objects = [];
  for (const post of posts) {

    if (!_.some(objects, function (currentPost) { return post.id === currentPost.id })) {
      objects.push({
        id: post.id,
        title: post.title,
        body: post.body,
        user_id: post['user.id'],
        username: post['user.name'],
        user_avatar: post['user.imageUrl'],
        email: post['user.email'],
        images: [],
        created_at: post.created_at,
        updated_at: post.updated_at,
      });
    }

    objects = _.map(objects, function (currentPost) {
      if (currentPost.id === post.id && post['images.id'] !== null) {
        currentPost.images.push({
          id: post['images.id'],
          url: post['images.url']
        });
      }
      return currentPost;
    });

  }
  return _.first(objects);
}

module.exports = {
  getPosts: getPosts,
  getPost: getPost
};
