const _ = require('lodash');
//const createModel = require('../../src/models/posts.model');
const sessionMessagesModel = require('../../src/models/session-messages.model');
const socketioJwt = require('socketio-jwt');
let users = [];

module.exports = function (io, app) {
  io.on('connection', socketioJwt.authorize({
    secret: app.get('authentication').secret,
    timeout: 5000
  })).on('authenticated', function(socket) {
    //example of sockets
    users.push({
      userId: socket.decoded_token.userId,
      socketId: socket.id,
    });
    //const postModel = createModel(app);
    const sessionMessages = sessionMessagesModel(app);

    socket.on('message_server', function (data) {
      //check if user is in the chat

      let senderUserId = socket.decoded_token.userId;
      let sessionId = data.sessionId;
      let receiverId = data.userId;

      sessionMessages.create({
        sessionId: sessionId,
        senderId: senderUserId,
        receiverId: receiverId,
        message: data.message
      }).then(()=> {

      });

      let receiverUser = _.find(users, function(user) {
        return user.userId === receiverId;
      });

      if (_.isEmpty(receiverUser)) {
        //todo - send push notification to user
      } else {
        socket.broadcast.to(receiverUser.socketId).emit('message_client', {
          message: data.message,
          userFrom: senderUserId,
          sessionId: data.sessionId
        });
      }

      return true;
    });

    socket.on('disconnect', function () {
      _.remove(users, function(user) {
        return user.socketId === socket.id;
      });
    });

  });
};
