const multer = require('multer');
const multipartMiddleware = multer();

module.exports = function () {
  const app = this;
  app.use(
     multipartMiddleware.array('images'),
    (req,res,next) =>{
      req['feathers'].files = req.files;
      next();
    });
}