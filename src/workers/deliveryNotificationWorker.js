//const pushService = require('../../server/utils/pushService');
//const config = require('../../config');
const kue = require('kue');
const path = require('path');
const redis = require('redis');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const deliveryNotificationsService = require('../utils/pushService');
const postgres = require('../postgres');
const app = feathers();
app.set('root', path.join(__dirname, '..'));
app.configure(configuration(app.get('root')));
app.configure(postgres);
app.configure(deliveryNotificationsService);

kue.redis.createClient = function() {
  return redis.createClient(app.get('redis').server.port, app.get('redis').server.host);
};

const Queue = kue.createQueue();

Queue.process('deliveryNotificationWorker', (job, done) => {
  let ids = job.data.ids;
  let title = job.data.title;
  let text = job.data.text;
  let token = job.data.token;
  let data = job.data.data;
  let recipients = [{
    token: token
  }];

  let message = {
    text: text,
    data: data
  };

  if (ids !== 'undefined') {
    message.ids = ids;
  }

  if (title !== 'undefined') {
    message.title = title;
  }

  const deliveryNotificationsService = app.get('deliveryNotificationsService');

  deliveryNotificationsService.send({
    message: message
  }, recipients);

  return done();

});

