//const config = require('../../config');
const kue = require('kue');
const _ = require('lodash');
const path = require('path');
const redis = require('redis');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const createDevicesModel = require('../../src/models/devices.model');
const createSessionsModel = require('../../src/models/sessions.model');
const postgres = require('../postgres');
const app = feathers();
const RedisNotifier = require('redis-notifier');
const client = redis.createClient();
app.set('root', path.join(__dirname, '..'));
app.configure(configuration(app.get('root')));
app.configure(postgres);

const Queue = kue.createQueue();

const eventNotifier = new RedisNotifier(redis, {
  redis: {host: app.get('redis').server.host, port: app.get('redis').server.port},
  expired: true,
  evicted: true,
  logLevel: 'DEBUG' //Defaults To INFO
});

const devicesModel = createDevicesModel(app);
const sessionsModel = createSessionsModel(app);

client.on("error", function (err) {
  console.log("Error " + err);
});

function NotifyProlongSession(data) {
  sessionsModel.findOne({
    where: {
      id: data.sessionId
    }
  }).then((session) => {
    if (session.isCancelled) {
      return false;
    }

    return devicesModel.getUserDevicesById(data.ownerId).then(devices => {
      if (devices === null) {
        return false;
      }

      _.forEach(devices, function (device) {
        let options = {
          title: "You can prolong session",
          text: "You can prolong session",
          token: device.token,
          data: {
            sessionId: data.sessionId
          }
        };

        Queue.create('deliveryNotificationWorker', options).save();
      });

      return true;
    })
  });
}

function NotifyBeforeWalk(data) {
  sessionsModel.findOne({
    where: {
      id: data.sessionId
    }
  }).then((session) => {
    if (session.isCancelled) {
      return false;
    }

    return devicesModel.getUserDevicesById(data.providerId).then(devices => {
      if (devices === null) {
        return false;
      }

      _.forEach(devices, function (device) {
        let options = {
          title: "You should walk the dog approximately through 30 minutes",
          text: "You should walk the dog approximately through 30 minutes",
          token: device.token,
          data: {}
        };

        Queue.create('deliveryNotificationWorker', options).save();
      });

      return true;
    })
  });
}

eventNotifier.on('message', function (pattern, channelPattern, emittedKey) {
  const channel = this.parseMessageChannel(channelPattern);
  switch (channel.key) {
    case 'expired':

      let substring = "custom_";
      if (emittedKey.indexOf(substring) === -1) {
        return;
      }

      let key = JSON.parse(emittedKey.split(substring)[1]);
      switch (key.type) {
        case 'NotifyBeforeWalk':
          return NotifyBeforeWalk(key);
          break;
        case 'NotifyProlongSession':
          return NotifyProlongSession(key);
          break;
        default:
          return false;
      }

      break;
    case "evicted":
      break;
    default:
      logger.debug("Unrecognized Channel Type:" + channel.type);
  }
});
