const configDefault = require('./default.json');
const deltaProduction = require('./production.json');
const configProduction = Object.assign({}, configDefault, deltaProduction);

module.exports = {
  development: configDefault.db,
  production: configProduction.db
};
