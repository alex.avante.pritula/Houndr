'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('billing_types', [{
      id: 1,
      description: 'payment penalty for canceling session',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 2,
      description: 'payment for session by owner',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 3,
      description: 'payment for session to provider',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 4,
      description: 'payment for prolong session',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 5,
      description: 'refund after cancelling session',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
          delete from billing_types
          where id in (1,2,3,4,5)
          `, {
          type: Sequelize.QueryTypes.DELETE,
          transaction: transaction
        })
      ]);
    });
  }
};
