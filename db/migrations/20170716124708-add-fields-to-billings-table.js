'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'billings',
        'amount',
        {
          type: Sequelize.DECIMAL(11,2),
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      queryInterface.addColumn(
        'billings',
        'description',
        {
          type: Sequelize.TEXT,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'billings',
        'charge_id',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'billings',
        'transfer_id',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('billings', 'amount'),
        queryInterface.removeColumn('billings', 'description'),
        queryInterface.removeColumn('billings', 'charge_id'),
        queryInterface.removeColumn('billings', 'transfer_id'),
      ]);
    });
  }
};
