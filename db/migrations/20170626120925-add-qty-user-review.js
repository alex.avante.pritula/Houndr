'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'users',
        'count_reviews',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'id'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('users', 'count_reviews')
    ];
  }
};
