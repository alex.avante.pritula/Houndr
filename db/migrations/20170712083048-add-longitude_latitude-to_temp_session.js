'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'temp_sessions',
        'latitude',
        {
          type: Sequelize.DECIMAL,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'temp_sessions',
        'longitude',
        {
          type: Sequelize.DECIMAL,
          allowNull: true
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('temp_sessions', 'latitude'),
        queryInterface.removeColumn('temp_sessions', 'longitude'),
      ]);
    });
  }
};
