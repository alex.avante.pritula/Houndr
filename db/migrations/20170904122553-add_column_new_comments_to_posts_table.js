'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'posts',
      'new_comments',
      {
        type: Sequelize.BOOLEAN,
        allowEmpty: true,
        defaultValue: false
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('posts', 'new_comments');
  }
};
