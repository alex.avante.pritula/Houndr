'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('users', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        email: {
          type     : Sequelize.STRING,
          allowNull: true,
          unique   : true
        },

        password: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        google_id: { 
          type     : Sequelize.STRING,
          allowNull: true 
        },

        facebook_id: { 
          type     : Sequelize.STRING,
          allowNull: true
        },

        name: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        is_verified: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: false
        },

        age: {
          type      : Sequelize.INTEGER,
          allowEmpty: true
        },

        address: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        longitude: {
          type      : Sequelize.FLOAT,
          allowEmpty: true
        },

        latitude: {
          type      : Sequelize.FLOAT,
          allowEmpty: true
        },

        is_provider: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: false
        },

        has_notifications: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: true
        },

        image_url: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        avalable_from: {
          type      : Sequelize.DATE,
          allowEmpty: true
        },

        avalable_to: {
          type      : Sequelize.DATE,
          allowEmpty: true
        },

        last_activity: {
          type      : Sequelize.DATE,
          allowEmpty: true
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      })
        .then(() => {
          return queryInterface.sequelize.query(`
            CREATE INDEX email ON users (email);
          `, {
            type: Sequelize.QueryTypes.RAW,
            transaction: transaction
          });
        });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('users');
  }
};
