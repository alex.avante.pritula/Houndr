'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('session_messages', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        session_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'sessions',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        sender_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        receiver_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        message: {
          type: Sequelize.STRING,
          allowEmpty: true
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('session_messages');
  }
};
