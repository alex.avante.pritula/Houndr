'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('pets_sessions', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        pet_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'pets',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        session_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'sessions',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      })
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('pets_sessions');
  }
};
