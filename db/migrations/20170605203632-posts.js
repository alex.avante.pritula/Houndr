'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('posts', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },
        
        title: {
          type      : Sequelize.STRING,
          allowEmpty: false
        },

        text: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },    
        
        user_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },
        
        image_url: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('posts');
  }
};
