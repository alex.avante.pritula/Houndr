'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('billings_sessions', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        session_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'sessions',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        billing_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'billings',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query('CREATE UNIQUE INDEX ON billings_sessions (session_id, billing_id);', {
          type: Sequelize.QueryTypes.RAW,
          transaction: transaction
        });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('billings_sessions');
  }
};
