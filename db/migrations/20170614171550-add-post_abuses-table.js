'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('post_abuses', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        user_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        post_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'posts',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        text: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    return queryInterface.dropTable('post_abuses');
  }
};
