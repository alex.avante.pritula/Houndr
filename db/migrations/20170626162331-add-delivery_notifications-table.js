'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('delivery_notifications', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        push_type: {
          type: Sequelize.INTEGER,
          allowEmpty: false
        },

        data: {
          type: Sequelize.TEXT,
          allowEmpty: true
        },

        device_id: {
          type: Sequelize.STRING,
          allowEmpty: false
        },

        is_sent: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },

        delivery_date_time: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('delivery_notifications');
  }
};
