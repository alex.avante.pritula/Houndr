'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('pets', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        name: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        breed: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        sex: {
          type      :  Sequelize.ENUM,
          values    : ['male', 'female'],
          allowEmpty: true
        },

        is_vactinated: {
          type        : Sequelize.STRING,
          allowNull   : false,
          defaultValue: false,
        },

        image_url: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        user_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('pets');
  }
};
