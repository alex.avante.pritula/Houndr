'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'users',
        'twitter_id',
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'facebook_id'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('users', 'twitter_id')
    ];
  }
};
