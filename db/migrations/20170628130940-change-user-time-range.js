'use strict';

module.exports = {
  up: function (queryInterface , Sequelize ) {
    return [
      queryInterface.changeColumn(
        'users',
        'avalable_from',
        {
          type: Sequelize.TIME,
          allowEmpty: true
        }
      ),
      queryInterface.changeColumn(
        'users',
        'avalable_to',
        {
          type: Sequelize.TIME,
          allowEmpty: true
        }
      )
    ];
  },

  down: function (queryInterface , Sequelize ) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
          ALTER TABLE users ALTER COLUMN avalable_from
            TYPE timestamp USING ('2017-1-1'::date + avalable_from)
          `, {
          type: Sequelize.QueryTypes.RAW,
          transaction: transaction
        }),
        queryInterface.sequelize.query(`
            ALTER TABLE users ALTER COLUMN avalable_to
             TYPE timestamp USING ('2017-1-1'::date + avalable_to)
         `, {
          type: Sequelize.QueryTypes.RAW,
          transaction: transaction
        })
      ]);
    });
  }
};
