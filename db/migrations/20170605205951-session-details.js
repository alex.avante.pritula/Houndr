'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('session_details', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        session_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'sessions',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        user_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        comment: {
          type      : Sequelize.STRING,
          allowEmpty: true
        },

        rating: {
          type      : Sequelize.INTEGER,
          allowEmpty: true
        },

        account_type: {
          type      :  Sequelize.ENUM,
          values    : ['owner', 'provider'],
          allowEmpty: false
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('session_details');
  }
};
