'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('sessions', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        tax: {
          type: Sequelize.FLOAT(9, 2),
          allowEmpty: false
        },

        start_point: {
          type: Sequelize.STRING,
          allowEmpty: true
        },

        is_cancelled: {
          type: Sequelize.BOOLEAN,
          allowEmpty: false,
          defaultValue: false
        },

        longitude: {
          type: Sequelize.FLOAT,
          allowEmpty: true
        },

        latitude: {
          type: Sequelize.FLOAT,
          allowEmpty: true
        },

        owner_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        provider_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('sessions');
  }
};
