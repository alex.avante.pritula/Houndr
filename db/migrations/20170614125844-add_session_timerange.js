'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'sessions',
        'end_date_time',
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'id'
        }
      ),
      queryInterface.addColumn(
        'sessions',
        'start_date_time',
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'id'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize */) {
    return [
      queryInterface.removeColumn('sessions', 'end_date_time'),
      queryInterface.removeColumn('sessions', 'start_date_time')
    ];
  }
};
