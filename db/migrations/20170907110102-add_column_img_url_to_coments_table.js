'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'post_comments',
      'img_url',
      {
        type: Sequelize.STRING,
        allowEmpty: true,
        defaultValue: false
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('post_comments', 'img_url');
  }
};
