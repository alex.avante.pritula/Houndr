'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('settings', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },
        
        key: {
          type      : Sequelize.STRING,
          allowEmpty: false,
        },

        value: {
          type      : Sequelize.STRING,
          allowEmpty: false,
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('settings');
  }
};
