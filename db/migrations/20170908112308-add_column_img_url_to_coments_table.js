
'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn(
      'post_comments',
      'img_url',
      {
        type: Sequelize.STRING,
        defaultValue:null
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn(
      'post_comments',
      'img_url',
      {
        type: Sequelize.STRING,
        defaultValue:false
      }
    );
  }
};
