'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('billings', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        type_id: {
          type      : Sequelize.INTEGER,
          allowEmpty: false
        },

        user_id: {
          type      : Sequelize.INTEGER,
          allowEmpty: false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        }, 

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    }); 
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('billings');
  }
};
