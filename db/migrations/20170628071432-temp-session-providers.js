'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('temp_session_providers', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        provider_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        temp_session_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'temp_sessions',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        status: {
          type: Sequelize.ENUM,
          values: ['await', 'agreed', 'rejected'],
          allowEmpty: true
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('temp_session_providers');
  }
};
