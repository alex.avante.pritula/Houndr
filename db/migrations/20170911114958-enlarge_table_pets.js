'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'pets',
        'age',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      queryInterface.addColumn(
        'pets',
        'dogs_characteristic',
        {
          type: Sequelize.STRING(500),
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'pets',
        'dogs_instruction',
        {
          type: Sequelize.STRING(500),
          allowEmpty: true,
          defaultValue: ''
        }
      ),

      ////FUN
      queryInterface.addColumn(
        'pets',
        'fun_Energy',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      queryInterface.addColumn(
        'pets',
        'fun_Training',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      queryInterface.addColumn(
        'pets',
        'fun_Offleash',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue:0
        }
      ),
      queryInterface.addColumn(
        'pets',
        'fun_Kids',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      queryInterface.addColumn(
        'pets',
        'fun_Cats',
        {
          type: Sequelize.INTEGER,
          allowEmpty: true,
          defaultValue: 0
        }
      ),
      ////FUN

      ///VET

      queryInterface.addColumn(
        'pets',
        'vet_name',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'pets',
        'vet_location',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'pets',
        'vet_phone',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('pets', 'age'),
        queryInterface.removeColumn('pets', 'dogs_characteristic'),
        queryInterface.removeColumn('pets', 'dogs_instruction'),
        queryInterface.removeColumn('pets', 'fun_Energy'),
        queryInterface.removeColumn('pets', 'fun_Training'),
        queryInterface.removeColumn('pets', 'fun_Offleash'),
        queryInterface.removeColumn('pets', 'fun_Kids'),
        queryInterface.removeColumn('pets', 'fun_Cats'),
        queryInterface.removeColumn('pets', 'vet_name'),
        queryInterface.removeColumn('pets', 'vet_location'),
        queryInterface.removeColumn('pets', 'vet_phone'),

      ]);
    });
  }
};

