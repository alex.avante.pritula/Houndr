'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.renameColumn('users', 'avalable_from', 'available_as_owner'),
        queryInterface.renameColumn('users', 'avalable_to', 'available_as_provider')
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.renameColumn('users', 'available_as_provider', 'avalable_to'),
        queryInterface.renameColumn('users', 'available_as_owner', 'avalable_from'),

      ]);
    });
  }
};
