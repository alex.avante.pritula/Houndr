'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'posts',
      'post_code',
      {
        type: Sequelize.STRING,
        allowEmpty: true,
        defaultValue: ''
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('posts', 'post_code');
  }
};
