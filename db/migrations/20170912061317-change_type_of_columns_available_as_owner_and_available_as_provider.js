'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.changeColumn(
          'users',
          'available_as_owner',
          {
            type: Sequelize.STRING,
            allowNull: true,

          }
        ),
        queryInterface.changeColumn(
          'users',
          'available_as_provider',
          {
            type: Sequelize.STRING,
            allowNull: true,
          }
        )
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.changeColumn(
          'users',
          'available_as_owner',
          {
            type: Sequelize.TIME,
            allowNull: true,
          }
        ),
        queryInterface.changeColumn(
          'users',
          'available_as_provider',
          {
            type: Sequelize.TIME,
            allowNull: true,
          }
        )
      ]);
    });
  }
};
