'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('admins', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        email: {
          type      : Sequelize.STRING,
          allowEmpty: false,
          unique    : true
        },

        password: {
          type      : Sequelize.STRING,
          allowEmpty: false
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    }); 
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('admins');
  }
};
