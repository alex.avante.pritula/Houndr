'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('pets_temp_sessions', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        pet_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'pets',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        temp_session_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'temp_sessions',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query('CREATE UNIQUE INDEX ON pets_temp_sessions (pet_id, temp_session_id);', {
          type: Sequelize.QueryTypes.RAW,
          transaction: transaction
        });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('pets_temp_sessions');
  }
};
