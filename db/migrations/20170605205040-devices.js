'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('devices', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        device_id: {
          type      : Sequelize.STRING,
          allowEmpty: false
        },

        token: {
          type      : Sequelize.STRING,
          allowEmpty: false,
          require   : true
        },

        user_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model : 'users',
            key   : 'id'
          },
          onUpdate  : 'cascade',
          onDelete  : 'cascade'
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query('CREATE UNIQUE INDEX ON devices (token, device_id);', {
          type: Sequelize.QueryTypes.RAW,
          transaction: transaction
        });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('devices');
  }
};
