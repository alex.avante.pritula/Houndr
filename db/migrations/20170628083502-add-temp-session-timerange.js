'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'temp_sessions',
        'end_date_time',
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'id'
        }
      ),
      queryInterface.addColumn(
        'temp_sessions',
        'start_date_time',
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'id'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize */) {
    return [
      queryInterface.removeColumn('temp_sessions', 'end_date_time'),
      queryInterface.removeColumn('temp_sessions', 'start_date_time')
    ];
  }
};
