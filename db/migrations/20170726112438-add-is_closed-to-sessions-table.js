'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'sessions',
        'is_closed',
        {
          type: Sequelize.BOOLEAN,
          allowEmpty: false,
          defaultValue: false
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('sessions', 'is_closed')
      ]);
    });
  }
};
