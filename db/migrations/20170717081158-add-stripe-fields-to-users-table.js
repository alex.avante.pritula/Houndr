'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'users',
        'stripe_customer_id',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'users',
        'stripe_account_id',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('users', 'stripe_customer_id'),
        queryInterface.removeColumn('users', 'stripe_account_id')
      ]);
    });
  }
};
