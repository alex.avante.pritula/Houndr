'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'users',
        'about_myself',
        {
          type: Sequelize.STRING(500),
          allowEmpty: true,

        }
      ),
      queryInterface.addColumn(
        'users',
        'phone',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('users', 'about_myself'),
        queryInterface.removeColumn('users', 'phone'),


      ]);
    });
  }
};

