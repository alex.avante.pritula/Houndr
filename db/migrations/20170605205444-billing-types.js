'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('billing_types', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        description : {
          type: Sequelize.STRING,
          unique: true
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }

      }, {
        transaction: transaction
      });
    }); 
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('billing_types');
  }
};
