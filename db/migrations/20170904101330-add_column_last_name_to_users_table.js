'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'users',
        'last_name',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),
      queryInterface.addColumn(
        'users',
        'is_have_dog',
        {
          type: Sequelize.BOOLEAN,
          allowEmpty: true,
          defaultValue: false
        }
      ),
      queryInterface.addColumn(
        'users',
        'post_code',
        {
          type: Sequelize.STRING,
          allowEmpty: true,
          defaultValue: ''
        }
      ),


    ];
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.removeColumn('users', 'last_name'),
        queryInterface.removeColumn('users', 'post_code'),
        queryInterface.removeColumn('users', 'is_have_dog')
      ]);
    });
  }
};
