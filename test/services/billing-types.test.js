const assert = require('assert');
const app = require('../../src/app');

describe('\'billing-types\' service', () => {
  it('registered the service', () => {
    const service = app.service('billing-types');

    assert.ok(service, 'Registered the service');
  });
});
