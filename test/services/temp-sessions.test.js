const assert = require('assert');
const app = require('../../src/app');

describe('\'temp-sessions\' service', () => {
  it('registered the service', () => {
    const service = app.service('temp-sessions');

    assert.ok(service, 'Registered the service');
  });
});
