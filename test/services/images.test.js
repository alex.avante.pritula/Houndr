const assert = require('assert');
const app = require('../../src/app');

describe('\'images\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/images/:target');
    const dropService = app.service('api/v1/images/:target/name/:name');

    assert.ok(service, 'Registered the service');
    assert.ok(dropService, 'Registered the dropService');
  });
});
