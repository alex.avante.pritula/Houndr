const assert = require('assert');
const app = require('../../src/app');

describe('\'session-messages\' service', () => {
  it('registered the service', () => {
    const service = app.service('session-messages');

    assert.ok(service, 'Registered the service');
  });
});
