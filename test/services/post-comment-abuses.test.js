const assert = require('assert');
const app = require('../../src/app');

describe('\'post-comment-abuses\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/posts/comments/abuses');
    const abusesCertainPostCommentService = app.service('api/v1/posts/comments/:post_comment_id/abuses');

    assert.ok(service, 'Registered the service');
    assert.ok(abusesCertainPostCommentService, 'Registered the abusesCertainPostCommentService');
  });
});
