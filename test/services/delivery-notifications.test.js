const assert = require('assert');
const app = require('../../src/app');

describe('\'delivery-notifications\' service', () => {
  it('registered the service', () => {
    const service = app.service('delivery-notifications');

    assert.ok(service, 'Registered the service');
  });
});
