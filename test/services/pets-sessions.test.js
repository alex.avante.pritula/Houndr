const assert = require('assert');
const app = require('../../src/app');

describe('\'pets_sessions\' service', () => {
  it('registered the service', () => {
    const service = app.service('pets-sessions');

    assert.ok(service, 'Registered the service');
  });
});
