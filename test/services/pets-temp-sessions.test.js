const assert = require('assert');
const app = require('../../src/app');

describe('\'pets-temp-sessions\' service', () => {
  it('registered the service', () => {
    const service = app.service('pets-temp-sessions');

    assert.ok(service, 'Registered the service');
  });
});
