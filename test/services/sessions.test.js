const assert = require('assert');
const app = require('../../src/app');

describe('\'sessions\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/v1/sessions');
    const reviewService = app.service('/api/v1/sessions/:id/review');
    const typeService = app.service('/api/v1/sessions/type/:type');

    assert.ok(service, 'Registered the service');
    assert.ok(reviewService, 'Registered the reviewService');
    assert.ok(typeService, 'Registered the typeService');
  });
});
