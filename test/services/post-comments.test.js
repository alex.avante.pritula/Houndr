const assert = require('assert');
const app = require('../../src/app');

describe('\'post-comments\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/posts/comments');
    const commentsPostService = app.service('api/v1/posts/:post_id/comments');

    assert.ok(service, 'Registered the service');
    assert.ok(commentsPostService, 'Registered the commentsPostService');
  });
});
