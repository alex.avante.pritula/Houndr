const assert = require('assert');
const app = require('../../src/app');

describe('\'billings-sessions\' service', () => {
  it('registered the service', () => {
    const service = app.service('billings-sessions');

    assert.ok(service, 'Registered the service');
  });
});
