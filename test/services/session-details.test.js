const assert = require('assert');
const app = require('../../src/app');

describe('\'session-details\' service', () => {
  it('registered the service', () => {
    const service = app.service('session-details');

    assert.ok(service, 'Registered the service');
  });
});
