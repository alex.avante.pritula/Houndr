const assert = require('assert');
const app = require('../../src/app');

describe('\'post-likes\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/posts/likes');
    const postLikeService = app.service('api/v1/posts/:post_id/likes');
    const postUserLikeService = app.service('api/v1/posts/:post_id/users/:user_id/likes');
    const userLikeService = app.service('api/v1/users/:user_id/likes');

    assert.ok(service, 'Registered the service');
    assert.ok(postLikeService, 'Registered the postLikeService');
    assert.ok(postUserLikeService, 'Registered the postUserLikeService');
    assert.ok(userLikeService, 'Registered the userLikeService');
  });
});
