const assert = require('assert');
const app = require('../../src/app');

describe('\'temp-session-providers\' service', () => {
  it('registered the service', () => {
    const service = app.service('temp-session-providers');

    assert.ok(service, 'Registered the service');
  });
});
