const assert = require('assert');
const app = require('../../src/app');

describe('\'post-abuses\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/posts/abuses');
    const abusesCertainPostService = app.service('api/v1/posts/:post_id/abuses');

    assert.ok(service, 'Registered the service');
    assert.ok(abusesCertainPostService, 'Registered the abusesCertainPostService');
  });
});
