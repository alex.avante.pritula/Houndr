const assert = require('assert');
const app = require('../../src/app');

describe('\'pets\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/v1/pets');
    const servicePetImage = app.service('/api/v1/pets/:id/image');

    assert.ok(service, 'Registered the service');
    assert.ok(servicePetImage, 'Registered the servicePetImage');
  });
});
