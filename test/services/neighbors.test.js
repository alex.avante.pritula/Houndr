const assert = require('assert');
const app = require('../../src/app');

describe('\'neighbors\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/v1/neighbors');

    assert.ok(service, 'Registered the service');
  });
});
