const assert = require('assert');
const app = require('../../src/app');

describe('\'session-activities\' service', () => {
  it('registered the service', () => {
    const service = app.service('session-activities');

    assert.ok(service, 'Registered the service');
  });
});
