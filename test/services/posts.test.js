const assert = require('assert');
const app = require('../../src/app');

describe('\'posts\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/v1/posts');
    const mainImageService = app.service('/api/v1/posts/:id/image/main');
    const attachedImagesService = app.service('/api/v1/posts/:id/image/attached');

    assert.ok(service, 'Registered the service');
    assert.ok(mainImageService, 'Registered the mainImageService');
    assert.ok(attachedImagesService, 'Registered the attachedImagesService');
  });
});
