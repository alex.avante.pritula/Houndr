const assert = require('assert');
const app = require('../../src/app');

describe('\'post-images\' service', () => {
  it('registered the service', () => {
    const service = app.service('post-images');

    assert.ok(service, 'Registered the service');
  });
});
