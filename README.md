# houndr-backend

> 

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/houndr-backend; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g feathers-cli             # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

## Generator

    ``` 
    npm run generate-data/generate-data-prod <type> <count> [<params>]
    ``` 
    types: 
       $ all
         Example: `npm run generate-data all 10`

       $ users
         Example:  `npm run generate-data users 10`
         (!Important) `user.password === user.name`

       $ pets  [<userId>]
         Example: `npm run generate-data pets 10 1`

       $ sessions [<tax>, <ownerId>, <providerId>, <petsIds>]
         Example: `npm run generate-data all 10 5.5 2 3 1 1`

       $ posts [<userId>]
         Example: `npm run generate-data all posts 1`

       $ favorites [<userId>, <favoriteIds>]
         Example: `npm run generate-data all favorites 1 2 3`

  
## Redis
  for redis notifications you should set notify-keyspace-events:
    redis-cli config set notify-keyspace-events KEA
  
  for checking you set notifications:
    redis-cli config get notify-keyspace-events
  
  you should get:
    1) "notify-keyspace-events"
    2) "AKE"

## Preinstall on server
1. sudo apt-get install libpq-dev
2. sudo npm i -g pg-native

## Payment system
1. Get billings for session
  http://server_host:3030/api/v1/sessions/{session_id}/billings
2. Get all billings
  http://server_host:3030/api/v1/billings
3. create or update customer(for payment from user to stripe account(it needs when owner pays for walking))
  http://server_host:3030/api/v1/users/customers
4. create or update account(for payment from stripe account to provider(after one day cron sends money to provider))
    http://server_host:3030/api/v1/users/accounts
    
# Communication with IOS
1. In the begin you create temp session
  [POST] http://server_host:3030/api/v1/temp-sessions/
  [BODY] {
             "startDateTime": 1498730710535,
             "endDateTime": 1498730969735,
             "timezone": "America/Los_Angeles",
             "latitude": 1.34,
             "longitude": 2.43
         }
2. Add temp pets to temp session.
  http://server_host:3030/api/v1/temp-sessions/{temp_session_id}/pets
  [POST] {
         	"pets": [
         		1,2,3
         	]
         }
3. You search provider for session. When system find provider(providers shouldn't be in another sessions or temp sessions) 
it saves them to temp_session_provider and return count of found providers. Also system sends push notification to providers.
If pass "isRestarted" to query all providers will be removed(it needs if you want restart searching of neighbors).
  http://server_host:3030/api/v1/temp-sessions/1/neighbors/  
4. Provider can agree on temp_session. In this API system checks if user has account id(for next transfer money to provider).
Also system checks time when temp provider was created(it needs if provider remembers too late about invite to temp session).
 If user agree system sends silent push to owner
  http://server_host:3030/api/v1/temp-sessions/1/providers
  [PATCH] {
          	"status": "agreed"
          }
5. When owner chooses provider he should pay session for that owner creates session. System checks has user or not customer id.
System connects pets from pets_temp_sessions to sessions. Also calculate and set redis notifications for notifying provider for 30 minutes before walking.
Calculate and set redis notifications for notifying owner for 30 minutes before end session.

  http://server_host:3030/api/v1/sessions
  [POST] {
         	"amount": 10,
         	"provider_id": 85,
         	"temp_session_id": 1
         }
6. Api for calculation of cost session
   http://server_host:3030/api/v1/sessions/amount
   [POST] {
            "start_date_time": 1501329600,
            "end_date_time": 1501339600
           } 
   [RESPONSE] {
                  "amount": 31.5
              }
Logic of calculation in specification - https://docs.google.com/document/d/1bPwYRAhf_uVBsVKzTLAAzDeSOWHLp1i9K67380Yer6o/edit?pli=1


## PM2
We have 3 workers:
1. base project
pm2 start npm --name "houndr_backend" -- run deploy
2. worker for push notifications
pm2 start --name deliveryNotificationsWorker src/workers/deliveryNotificationWorker.js
3. worker for redis notifications
pm2 start --name redisNotificationWorker src/workers/redisNotificationWorker.js

## Migrations
1. On the server should be environment "production"
export NODE_ENV=production
2. In root path of our project run:
node_modules/.bin/sequelize migration:create --name some-name-of-migration
3. Run migrate: npm run migrate


## Crons
1. 0 */1 * * * node "/home/ubuntu/src/commands/sessionPayment.js"
## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2016

Licensed under the [MIT license](LICENSE).
