$(document).ready(function () {
  $("#confirm-password").validate({
    rules: {
      password: {
        required: true,
        minlength: 4,
      },
    },
    messages: {
      password: {
        required: "Please provide a password",
        minlength: 'Password is too short',
        maxlength: 'Password is too long'
      },
    }
  });

  $('.btn-submit').click(function (e) {
    e.preventDefault();
    if ($("#confirm-password").valid()) {
      var $inputs = $('form').find('input');
      var data = {
        password: $('#password').val(),
        token: location.href.split('/').pop()
      }
      $.ajax({

        contentType: "application/json; charset=utf-8",
        url: "/api/v1/password",
        beforeSend: function () {
          $inputs.prop("disabled", true);
        },
        type: "put",
        data: JSON.stringify(data)
      })
        .done(function (response, textStatus, jqXHR) {
          alert('Your password has been updated');
          location.reload();
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          console.log('fail')
        }).always(function () {
        $inputs.prop("disabled", false);
      });
    }
  })
});
