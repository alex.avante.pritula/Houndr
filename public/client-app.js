feathersClient
  .configure(feathers.hooks())
  .configure(feathers.authentication({
    storage: window.localStorage
  }));


function authFirstUser() {
  var userToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyIsInR5cGUiOiJhY2Nlc3MifQ.eyJ1c2VySWQiOjEsImlhdCI6MTQ5ODQ2NzI2OSwiZXhwIjoxNDk4NTUzNjY5LCJhdWQiOiJodHRwczovL3lvdXJkb21haW4uY29tIiwiaXNzIjoiZmVhdGhlcnMiLCJzdWIiOiJhbm9ueW1vdXMifQ.MZVNjiUbFin1K7bYylydS2UHlyL8rkOHsMoSIXwut5k';
  authUser(userToken);
}

function authSecondUser() {
  var userToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyIsInR5cGUiOiJhY2Nlc3MifQ.eyJ1c2VySWQiOjIsImlhdCI6MTQ5ODQ2NzI5MCwiZXhwIjoxNDk4NTUzNjkwLCJhdWQiOiJodHRwczovL3lvdXJkb21haW4uY29tIiwiaXNzIjoiZmVhdGhlcnMiLCJzdWIiOiJhbm9ueW1vdXMifQ.JxOwO5gMPHtet_IEVO6jO0bAVKgeTZoY7rdBkqDFK1s'
  authUser(userToken);
}

function authUser(userToken) {
  socket.emit('authenticate', {
    // strategy: "local",
    // email: 'new_gulidovs9@gmail.com',
    // password: '115731'
    token: userToken
  });
}

//authFirstUser();
authSecondUser();


function sendMessageToSecondUser(message) {
  console.log('second');
  socket.emit('message_server', {userId: 2, sessionId: 1, message: message});
}

function sendMessageToFirstUser(message) {
  console.log(socket);
  socket.emit('message_server', {userId: 1, sessionId: 1, message: message});
}

//example of sockets
socket.on('authenticated', function (user) {

}).on('unauthorized', function(msg) {
    console.log("unauthorized: " + JSON.stringify(msg.data));
    throw new Error(msg.data.type);
});

socket.on('message_client', function (data) {
  console.log(data);
});
